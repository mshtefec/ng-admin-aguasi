export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCbPs193Gg0oA4iRDrfzQBHuk7AVHx_uJ8",
    authDomain: "ng-admin-aguasi.firebaseapp.com",
    databaseURL: "https://ng-admin-aguasi.firebaseio.com",
    projectId: "ng-admin-aguasi",
    storageBucket: "ng-admin-aguasi.appspot.com",
    messagingSenderId: "254488408347",
    appId: "1:254488408347:web:162020ce5d35ad05c963be",
    measurementId: "G-PJTE028M4E"
  },
};
