// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCbPs193Gg0oA4iRDrfzQBHuk7AVHx_uJ8",
    authDomain: "ng-admin-aguasi.firebaseapp.com",
    databaseURL: "https://ng-admin-aguasi.firebaseio.com",
    projectId: "ng-admin-aguasi",
    storageBucket: "ng-admin-aguasi.appspot.com",
    messagingSenderId: "254488408347",
    appId: "1:254488408347:web:162020ce5d35ad05c963be",
    measurementId: "G-PJTE028M4E"
  },
    web_api_url_base: 'http://localhost/api-rest-aguasi/public',
    // web_api_url_base: 'https://apirest.aguasi.com/public',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
