import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { SaleI }         from '@interfaces/sale.interface'

@Injectable({
  providedIn: 'root'
})
export class SaleService { 
    public url = environment.web_api_url_base + '/api/sales';

    constructor(private http: HttpClient) { }

    getSales(): Observable<SaleI[]> {
        return this.http.get<SaleI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }
    
    addSale(sale: SaleI): Observable<SaleI> {
        return this.http.post(this.url + '/new', sale);
    }
}