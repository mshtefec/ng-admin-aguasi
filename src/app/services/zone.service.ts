import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { ZoneI }          from '@interfaces/zone.interface';

@Injectable({
  providedIn: 'root'
})
export class ZoneService { 
    public url = environment.web_api_url_base + '/api/zones';

    constructor(private http: HttpClient) { }

    getAll(): Observable<ZoneI[]> {
        return this.http.get<ZoneI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getOne(id: number): Observable<ZoneI> {
        return this.http.get<ZoneI>(this.url + '/' + id);
    }
    
    add(zone: ZoneI): Observable<ZoneI> {
        return this.http.post(this.url + '/new', zone);
    }

    update(zone: ZoneI): Observable<ZoneI> {
        return this.http.put(this.url + '/edit/' + zone.id, zone);
    }

    deleted(id: number): Observable<ZoneI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}