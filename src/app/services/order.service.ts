import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { OrderI }         from '@interfaces/order.interface'

@Injectable({
  providedIn: 'root'
})
export class OrderService { 
    public url = environment.web_api_url_base + '/api/orders';

    constructor(private http: HttpClient) { }

    getOrders(): Observable<OrderI[]> {
        return this.http.get<OrderI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getAllWithNames(): Observable<any[]> {
        return this.http.get<any[]>(this.url + '/named').pipe( 
            map( data => { return data; })
        );
    }

    getOrder(id: number): Observable<any> {
        return this.http.get<OrderI>(this.url + '/' + id);
    }
    
    addOrder(order: OrderI): Observable<OrderI> {
        return this.http.post(this.url + '/new', order);
    }

    updateOrder(order: OrderI): Observable<OrderI> {
        return this.http.put(this.url + '/edit/' + order.id, order);
    }

    deletedOrder(id: number): Observable<OrderI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }

    sellOne(order: OrderI): Observable<OrderI> {
        return this.http.put(this.url + '/sell/' + order.id, order);
    }

    finishOne(order: OrderI): Observable<OrderI> {
        return this.http.put(this.url + '/finish/' + order.id, order);
    }
}