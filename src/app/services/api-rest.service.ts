import { Injectable, Inject }     from '@angular/core';
import { HttpClient }             from '@angular/common/http';

import { Observable }             from 'rxjs';
import { map }                    from 'rxjs/operators';

import { environment }            from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiRestService { 

    private data: string = '';

    private url = environment.web_api_url_base + '/api/';

    constructor(
        @Inject('path')
        private path: string,
        private http: HttpClient
    ) { }

    getAll(): Observable<any[]> {
        return this.http.get<any[]>(this.url + this.path).pipe( 
            map( data => { return data; })
        );
    }

    getAllFrom(path): Observable<any[]> {
        return this.http.get<any[]>(this.url + path).pipe( 
            map( data => { return data; })
        );
    }

    get(id: number): Observable<any> {
        return this.http.get<any>(this.url + this.path + '/' + id);
    }

    add(object: any): Observable<any> {
        return this.http.post(this.url + this.path + '/new', object);
    }

    update(object: any): Observable<any> {
        return this.http.put(this.url + this.path + '/edit/' + object.id, object);
    }

    deleted(id: number): Observable<any> {
        return this.http.delete(this.url + this.path + '/deleted/' + id);
    }

    searchBy(search: string): Observable<any[]> {
        return this.http.get<any[]>(this.url + this.path + '/search/' + search).pipe( 
            map( data => {
                return data;
            })
        );
    }
}