import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { ClientStatusI }  from '@app/interfaces/client-status.interface';

@Injectable({
  providedIn: 'root'
})
export class ClientStatusService { 
    public url = environment.web_api_url_base + '/api/client-status';

    constructor(private http: HttpClient) { }

    getAll(): Observable<ClientStatusI[]> {
        return this.http.get<ClientStatusI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getOne(id: number): Observable<ClientStatusI> {
        return this.http.get<ClientStatusI>(this.url + '/' + id);
    }
    
    add(statususer: ClientStatusI): Observable<ClientStatusI> {
        return this.http.post(this.url + '/new', statususer);
    }

    update(statususer: ClientStatusI): Observable<ClientStatusI> {
        return this.http.put(this.url + '/edit/' + statususer.id, statususer);
    }

    deleted(id: number): Observable<ClientStatusI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}