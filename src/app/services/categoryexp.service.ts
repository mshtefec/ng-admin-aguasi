import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { CategoryExpI }   from '@interfaces/categoryexp.interface'

@Injectable({
  providedIn: 'root'
})
export class CategoryExpService { 
    public url = environment.web_api_url_base + '/api/expenses/categories';

    constructor(private http: HttpClient) { }

    getCategoriesExp(): Observable<CategoryExpI[]> {
        return this.http.get<CategoryExpI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getCategoryExp(id: number): Observable<CategoryExpI> {
        return this.http.get<CategoryExpI>(this.url + '/' + id);
    }

    addCategoryExp(categoryexp: CategoryExpI): Observable<CategoryExpI> {
        return this.http.post(this.url + '/new', categoryexp);
    }

    updateCategoryExp(categoryexp: CategoryExpI): Observable<CategoryExpI> {
        return this.http.put(this.url + '/edit/' + categoryexp.id, categoryexp);
    }

    deletedCategoryExp(id: number): Observable<CategoryExpI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}