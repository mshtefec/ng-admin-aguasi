import { Injectable }           from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument }     from '@angular/fire/firestore';

import { UserI }                                        from '@app/interfaces/user.interface'

import { Observable }           from 'rxjs';
import { map, finalize }        from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private path: string;

  constructor(
    private afs: AngularFirestore
  ) { }

  initConfig(path: string) {
    this.path = path;
  }

  getAll() {
    return this.afs.collection(this.path).valueChanges();
  }

  getAllByCollection(colection: string) {
    return this.afs.collection(colection).valueChanges();
  }

  getAllByCollectionAndQuery(colection: string, atribute: string, condition: string) {
    return this.afs.collection(colection, ref => ref.where(atribute, '==', condition)).valueChanges();
  }

  // devuelve los productos pero con el id asociado a cada documento en la colección.
  getAllWithId(): Observable<any[]> {
    return this.afs
      .collection(this.path)
      .snapshotChanges()
      .pipe(
        map(actions => 
          actions.map(a => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  getAllWithIdAndQuery(atribute: string, condition: string): Observable<any[]> {
    return this.afs
      .collection(this.path, ref => ref.where(atribute, '==', condition))
      .snapshotChanges()
      .pipe(
        map(actions => 
          actions.map(a => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  getObject(id: string) {
    return this.afs.collection(this.path).doc(id).valueChanges();
  }

  getObjectByCollection(colection: string, id: string) {
    return this.afs.collection(colection).doc(id).valueChanges();
  }

  createObject(data: any) {
    return this.afs.collection(this.path).add(data);
  }

  // obtengo la id que se va a crear en la colección dentro del objeto producto
  // actualizo el objeto seteando la id y guardo la colección pero con set() no add()
  // esta función permite optimizar el listado de productos
  // createObjectAndSetId(data: any) {
  //   const id = this.afs.createId();
  //   data.id = id;
  //   return this.afs.collection(this.path).doc(id).set(data);
  // }

  updateObject(id: string, data: any) {
    return this.afs.collection(this.path).doc(id).set(data);
  }

  deletedObject(data: any) {
    return this.afs.collection(this.path).doc(data.id).delete();
  }

  // updateUserInfo(id: string, user: UserI) {
  //   const userRef: AngularFirestoreDocument<UserI> = this.afs.doc(`users/${id}`);

  //   const data = {
  //     lastname: user.lastname,
  //     firstname: user.firstname,
  //     dni: user.dni,
  //     celphone: user.celphone,
  //     direction: user.direction
  //   }

  //   return userRef.set(data, { merge: true });
  // }

}
