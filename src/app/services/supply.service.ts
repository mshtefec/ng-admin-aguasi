import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { SupplyI }        from '@interfaces/supply.interface'

@Injectable({
  providedIn: 'root'
})
export class SupplyService { 
    public url = environment.web_api_url_base + '/api/supplies';

    constructor(private http: HttpClient) { }

    getSupplies(): Observable<SupplyI[]> {
        return this.http.get<SupplyI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getSupply(id: number): Observable<SupplyI> {
        return this.http.get<SupplyI>(this.url + '/' + id);
    }

    addSupply(supply: SupplyI): Observable<SupplyI> {
        return this.http.post(this.url + '/new', supply);
    }

    updateSupply(supply: SupplyI): Observable<SupplyI> {
        return this.http.put(this.url + '/edit/' + supply.id, supply);
    }

    deletedSupply(id: number): Observable<SupplyI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}