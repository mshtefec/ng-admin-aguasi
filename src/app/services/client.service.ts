import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map, filter }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { ClientI }        from '@interfaces/client.interface'

@Injectable({
  providedIn: 'root'
})
export class ClientService { 
    public url = environment.web_api_url_base + '/api/clients';

    constructor(private http: HttpClient) { }

    getClientsOrderByLastname(): Observable<ClientI[]> {
        return this.http.get<ClientI[]>(this.url + '/orderByLastname').pipe( 
            map( data => {
                return data;
            })
        );
    }

    getClients(): Observable<ClientI[]> {
        return this.http.get<ClientI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getAllZoned(): Observable<any[]> {
        return this.http.get<any[]>(this.url + '/zones').pipe( 
            map( data => { return data; })
        );
    }

    getClient(id: number): Observable<ClientI> {
        return this.http.get<ClientI>(this.url + '/' + id);
    }

    getClientsOfSeller(id: string): Observable<ClientI[]> {
        return this.http.get<ClientI[]>(this.url).pipe( 
            map( data => {
                let filter = data.filter(d => d.seller == id);
                return filter;
            })
        );
    }
    
    addClient(client: ClientI): Observable<ClientI> {
        return this.http.post(this.url + '/new', client);
    }

    updateClient(client: ClientI): Observable<ClientI> {
        return this.http.put(this.url + '/edit/' + client.id, client);
    }

    deletedClient(id: number): Observable<ClientI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }

}