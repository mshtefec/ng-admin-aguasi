import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '@environments/environment';

import { ClientOriginI }  from '@app/interfaces/client-origin.interface';

@Injectable({
  providedIn: 'root'
})
export class ClientOriginService { 
    public url = environment.web_api_url_base + '/api/client-origin';

    constructor(private http: HttpClient) { }

    getAll(): Observable<ClientOriginI[]> {
        return this.http.get<ClientOriginI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getOne(id: number): Observable<ClientOriginI> {
        return this.http.get<ClientOriginI>(this.url + '/' + id);
    }
    
    add(originuser: ClientOriginI): Observable<ClientOriginI> {
        return this.http.post(this.url + '/new', originuser);
    }

    update(originuser: ClientOriginI): Observable<ClientOriginI> {
        return this.http.put(this.url + '/edit/' + originuser.id, originuser);
    }

    deleted(id: number): Observable<ClientOriginI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}