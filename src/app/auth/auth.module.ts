import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { ReactiveFormsModule }      from '@angular/forms';
import { AuthRoutingModule }        from '@app/auth/auth-routing.module';
import { AuthComponent }            from '@app/auth/auth.component';
import { LoginComponent }           from '@app/auth/login/login.component';
import { NbEvaIconsModule }         from '@nebular/eva-icons';  

import { FormsModule }              from '@angular/forms';
import { HttpClientModule }         from '@angular/common/http';
import { NgxSpinnerModule }         from 'ngx-spinner';

import {
  NbThemeModule,
  NbLayoutModule,
  NbIconModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbInputModule,
  NbFormFieldModule
} from '@nebular/theme';

import { AuthService }              from '@app/services/auth.service';


@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    
    NbEvaIconsModule,
    NbThemeModule.forRoot(),
    NbLayoutModule,
    NbInputModule,
    NbFormFieldModule,
    NbIconModule,
    NbButtonModule,
    NbCardModule,
    NbTabsetModule,

    NgxSpinnerModule

  ],
  declarations: [
    AuthComponent,
    LoginComponent
  ],
  providers: [
    AuthService
  ],
})
export class AuthModule { }
