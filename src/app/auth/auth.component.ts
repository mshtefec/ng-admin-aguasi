import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  template: `
    <router-outlet></router-outlet>
  `,
  providers: []
})
export class AuthComponent implements OnInit {

  constructor( 
  ) {
  }

  ngOnInit() {
  }

}