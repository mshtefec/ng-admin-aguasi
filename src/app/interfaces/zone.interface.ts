export interface ZoneI {
    id?: number;
    name?: string;
    description?: string;
}