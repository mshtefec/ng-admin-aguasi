import { SupplyI } from 'src/app/interfaces/supply.interface';

export interface PromoI {
    id?: string;
    description?: string;
    supplies?: SupplyI[];
    total?: number;
    active?: boolean;
    created_at?: Date;
}