import { ClientOriginI }    from './client-origin.interface';
import { ClientStatusI }    from './client-status.interface';
import { UserI }            from './user.interface';
import { ZoneI }            from './zone.interface';
import { OrderI }           from './order.interface';

export interface ClientI {
    id?: string;
    lastname?: string;
    firstname?: string;
    dni?: number;
    celphone?: number;
    address?: string;
    email?: string;
    debe?: number;
    province?: string;
    locality?: string;
    cp?: string;
    description?: string;
    seller?: UserI;
    zone?: ZoneI;
    high_date?: Date;
    status?: ClientStatusI;
    origin?: ClientOriginI;
    orders?: OrderI[];
}