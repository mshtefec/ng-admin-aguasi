export interface ClientStatusI {
    id?: number;
    name?: string;
    description?: string;
}