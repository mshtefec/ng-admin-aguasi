import { ClientI } from 'src/app/interfaces/client.interface';
import { SupplyI } from 'src/app/interfaces/supply.interface';
import { UserI } from 'src/app/interfaces/user.interface';

export interface OrderI {
    id?: number;
    client?: ClientI;
    seller?: UserI;
    delivery?: UserI;
    supplies?: SupplyI[];
    supply?: SupplyI;
    delivery_date?: Date;
    load_date?: Date;
    created_at?: Date;
    address?: string;
    description?: string;
    total?: number;
    status?: string;
    quantity?: number;
}