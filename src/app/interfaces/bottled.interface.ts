export interface BottledI {
    number?: string;
    liters?: number;
    status?: boolean;
}