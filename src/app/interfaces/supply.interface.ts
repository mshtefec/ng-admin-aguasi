import { CategoryExpI } from 'src/app/interfaces/categoryexp.interface';

export interface SupplyI {
    id?: number;
    name?: string;
    description?: string;
    price?: number;
    stock?: number;
    category?: CategoryExpI;
    type?: number;
    active?: number;
    created_at?: Date;
    updated_at?: Date;
}