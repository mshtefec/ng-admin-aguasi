export interface ClientOriginI {
    id?: number;
    name?: string;
    description?: string;
}