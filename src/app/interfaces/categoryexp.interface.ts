export interface CategoryExpI {
    id?: string;
    name?: string;
    description?: string;
}