export interface UserI {
    id?: string;
    username?: string;
    firstname?: string;
    lastname?: string;
    email?: string;
    password?: string;
    role?: string;
    get_token?: boolean;
    created_at?: Date;
    updated_at?: Date;
}