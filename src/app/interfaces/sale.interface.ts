import { ClientI } from 'src/app/interfaces/client.interface';
import { SupplyI } from 'src/app/interfaces/supply.interface';
import { UserI } from 'src/app/interfaces/user.interface';

export interface SaleI {
    id?: number;
    client?: ClientI;
    delivery?: UserI;
    supplies?: SupplyI[];
    delivery_date?: Date;
    address?: string;
    description?: string;
    total?: number;
    status?: string;
}