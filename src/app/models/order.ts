import { ClientI } from 'src/app/interfaces/client.interface';
import { SupplyI } from 'src/app/interfaces/supply.interface';
import { UserI } from 'src/app/interfaces/user.interface';

export class Order {

    constructor (
        public id: number,
        public client: ClientI,
        public seller: UserI,
        public delivery: UserI,
        public supply: SupplyI,
        public delivery_date: Date,
        public address: string,
        public total: number,
        public status: string,
        public quantity: number
    ) {}

}