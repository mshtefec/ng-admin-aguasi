import { NgModule }                     from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';
import { SellerComponent }              from './seller.component';

import { ClientListComponent }          from './pages/clients/client-list/client-list.component';
import { ClientNewComponent }           from './pages/clients/client-new/client-new.component';
import { ClientEditComponent }          from './pages/clients/client-edit/client-edit.component';
import { ClientShowComponent }          from './pages/clients/client-show/client-show.component';
import { OrderListComponent }           from './pages/orders/order-list/order-list.component';
import { OrderNewComponent }            from './pages/orders/order-new/order-new.component';
import { SaleListComponent }            from './pages/sales/sale-list/sale-list.component';

const routes: Routes = [
  { 
    path: '', 
    component: SellerComponent,
    children: [
      { path: 'clients',          component: ClientListComponent, },
      { path: 'clients/new',      component: ClientNewComponent },
      { path: 'clients/edit/:id', component: ClientEditComponent, },
      { path: 'clients/:id',      component: ClientShowComponent, },
      { path: 'orders',           component: OrderListComponent, },
      { path: 'orders/new',       component: OrderNewComponent, },
      { path: 'sales',            component: SaleListComponent, },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerRoutingModule { }
