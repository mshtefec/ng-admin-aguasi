import { Component, OnInit, ChangeDetectionStrategy }       from '@angular/core';
import { NbMenuItem, NbIconLibraries }                      from '@nebular/theme';
import { MenuTranslatorService }                            from '@services/menu-translator.service';
import { AuthService }                                      from '@app/services/auth.service';

@Component({
  selector: 'app-sidebar-seller',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <nb-menu [items]="items" autoCollapse="true">
    </nb-menu>
  `,
  providers: [MenuTranslatorService]
})
export class SidebarSellerComponent implements OnInit {

  private role: string;
  public items: NbMenuItem[];

  constructor(
    private translator: MenuTranslatorService,
    private iconLibraries: NbIconLibraries,
    public auth: AuthService
  ) {
    
    this.iconLibraries.registerFontPack('font-awesome', { packClass: 'fa', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('solid', { packClass: 'fas', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('light', {packClass: 'fal', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('duotone', {packClass: 'fad', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('brands', {packClass: 'fab', iconClassPrefix: 'fa'});
  }

  getDefaultMenu() {
    this.items = [
      {
        title: 'Mis Clientes',
        icon: 'people-outline',
        link: 'clients'
      },
      {
        title: 'Mis Ordenes',
        icon: 'car-outline',
        link: 'orders'
      },
      {
        title: 'Salir',
        icon: 'power-outline',
        link: '../logout/1'
      },
    ];
  }

  ngOnInit() {
    this.getDefaultMenu();
    this.items = this.translator.translate(this.items);
  }

}
