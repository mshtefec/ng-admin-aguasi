import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

//import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.scss']
})
export class SaleListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'client', 'delivery', 'total', 'delivery_date', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _orderService: OrderService,
    public router: Router,
    private toastr: ToastrService
  ) {
    
  }

  ngOnInit() {
    
    this._orderService.getOrders().subscribe(res => {
      // Aplico un filtro para los pedidos vendidos.
        const filter = res.filter(
          t => t.status == 'vendido'
        );
        this.dataSource.data = filter;
      }
    );
  }

  getTotal() {
    const sales: OrderI[] = this.dataSource.data;
    return sales.map(t => t.total).reduce((acc, value) => acc + value, 0);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
