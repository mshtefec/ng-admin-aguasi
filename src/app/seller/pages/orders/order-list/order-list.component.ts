import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'client', 'status', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _orderService: OrderService,
    public router: Router,
    private toastr: ToastrService
  ) {
    
  }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('identity'));
    
    this._orderService.getOrders().subscribe(res => {
      // Aplico un filtro para los pedidos del usuario, pero trae todos los pedidos.
        const filter = res.filter(
          t => t.seller == user.sub
        );
        this.dataSource.data = filter;

        // const assigned = filter.filter(
        //   o => o.status == 'PENDIENTE'
        // );
        // const finished = filter.filter(
        //   o => o.status == 'ENTREGADO'
        // );
        // this.countAssigned = assigned.length;
        // this.countFinished = finished.length;
      }
    );
  }

  getTotal() {
    const orders: OrderI[] = this.dataSource.data;
    return orders.map(t => t.total).reduce((acc, value) => acc + value, 0);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['seller/orders/new']);
  }

  onEdit(data: OrderI) {
    this.router.navigate(['seller/orders/form']);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._orderService.deletedOrder(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Orden borrada.',
              'success'
            )
            this.router.navigate(['seller/orders']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

}
