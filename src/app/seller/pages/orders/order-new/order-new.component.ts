import { Component, OnInit }                  from '@angular/core';
import { FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router }                             from '@angular/router';

import { OrderI }                             from 'src/app/interfaces/order.interface';
import { ClientI }                            from 'src/app/interfaces/client.interface';
import { UserI }                              from 'src/app/interfaces/user.interface';
import { SupplyI }                            from 'src/app/interfaces/supply.interface';

import { ToastrService }                      from 'ngx-toastr';
import { ClientService }                      from '@app/services/client.service';
import { UserService }                        from '@app/services/user.service';
import { SupplyService }                      from '@app/services/supply.service';
import { OrderService }                       from '@app/services/order.service';

import { Observable }                         from 'rxjs';
import { map, startWith, filter }                     from 'rxjs/operators';

@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.scss']
})
export class OrderNewComponent implements OnInit {

  orderId: string;
  totalOrder: number = 0;

  keyword = 'lastname';
  clients$: Observable<ClientI[]>;
  suppliesCollection: SupplyI[];

  clientInfo: ClientI;
  supplyInfo: SupplyI;

  orderForm = this.fb.group({
    client: ['', Validators.required],
    seller: ['', Validators.required],
    delivery: ['', Validators.required],
    delivery_date: ['', Validators.required],
    description: ['', [Validators.required, Validators.minLength(3)]],
    address: ['', Validators.required],
    total: [{value: '', disabled: true}, Validators.required],
    supply: ['', Validators.required],
    quantity: ['1',[Validators.min(0), Validators.required]]
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private _clientService: ClientService,
    private _userService: UserService,
    private _supplyService: SupplyService,
    private _orderService: OrderService
  ) {
  }

  ngOnInit() {

    let user = JSON.parse(localStorage.getItem('identity'));
    
    this.clients$ = this._clientService.getClientsOfSeller(user.sub);

    this._userService.getUser(2).subscribe( data => {
      this.orderForm.controls["delivery"].setValue(data);
    });
    this._userService.getUser(user.sub).subscribe( data => {
      this.orderForm.controls["seller"].setValue(data);
    });

    this._supplyService.getSupplies().subscribe( data => {
      this.suppliesCollection = data;
    });

  }

  onSubmit(form: any) {
    this.insertRecord(form);
  }

  insertRecord(form: any) {
    
    const order: OrderI = form;
    order.status = "CARGADO";
    order.total = this.totalOrder;

    this._orderService.addOrder(order).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Pedido Creado');
        this.router.navigate(['seller/orders']);
      },
      error => {
        console.error(error);
      }
    );

  }

  updateClientInfo(client: ClientI){
    this.clientInfo = client;
    this.orderForm.controls["address"].setValue(client.address);
    this.orderForm.controls["client"].setValue(client);
  }

  updateSupplyInfo(supply: SupplyI){
    this.supplyInfo = supply;
    this.totalOrder += Number(supply.price);
    this.orderForm.controls["total"].setValue(this.totalOrder);
  }

  updatePrice(quantity: Number){
    this.totalOrder = this.supplyInfo.price * Number(quantity);
    this.orderForm.controls["total"].setValue(this.totalOrder);
  }

}