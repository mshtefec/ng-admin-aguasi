import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }                             from '@angular/router';
import { ClientI }                            from '@interfaces/client.interface';
import { ToastrService }                      from 'ngx-toastr';
import { ClientService } from '@app/services/client.service';


@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {

  provinces: any = [
    { "name": "Ciudad Autónoma de Buenos Aires", "country": "AR" },
    { "name": "Buenos Aires", "country": "AR" },
    { "name": "Catamarca", "country": "AR" },
    { "name": "Chaco", "country": "AR" },
    { "name": "Chubut", "country": "AR" },
    { "name": "Córdoba", "country": "AR" },
    { "name": "Corrientes", "country": "AR" },
    { "name": "Entre Ríos", "country": "AR" },
    { "name": "Formosa", "country": "AR" },
    { "name": "Jujuy", "country": "AR" },
    { "name": "La Pampa", "country": "AR" },
    { "name": "La Rioja", "country": "AR" },
    { "name": "Mendoza", "country": "AR" },
    { "name": "Misiones", "country": "AR" },
    { "name": "Neuquén", "country": "AR" },
    { "name": "Río Negro", "country": "AR" },
    { "name": "Salta", "country": "AR" },
    { "name": "San Juan", "country": "AR" },
    { "name": "San Luis", "country": "AR" },
    { "name": "Santa Cruz", "country": "AR" },
    { "name": "Santa Fe", "country": "AR" },
    { "name": "Santiago del Estero", "country": "AR" },
    { "name": "Tierra del Fuego, Antártida e Islas del Atlántico Sur", "country": "AR" },
    { "name": "Tucumán", "country": "AR" }
  ]

  clientId: number;

  clientForm = new FormGroup({
    id: new FormControl(this.clientId, [Validators.required]),
    lastname: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    dni: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(8)])),
    celphone: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    debe: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    locality: new FormControl('', Validators.required),
    cp: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    seller: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _clientService: ClientService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.clientId = +params['id'];

        this._clientService.getClient(this.clientId).subscribe(
          response => {
            this.clientForm.controls.id.setValue(this.clientId);
            this.clientForm.controls.firstname.setValue(response.firstname);
            this.clientForm.controls.lastname.setValue(response.lastname);
            this.clientForm.controls.dni.setValue(response.dni);
            this.clientForm.controls.celphone.setValue(response.celphone);
            this.clientForm.controls.address.setValue(response.address);
            this.clientForm.controls.email.setValue(response.email);
            this.clientForm.controls.debe.setValue(response.debe);
            this.clientForm.controls.province.setValue(response.province);
            this.clientForm.controls.locality.setValue(response.locality);
            this.clientForm.controls.cp.setValue(response.cp);
            this.clientForm.controls.description.setValue(response.description);
            this.clientForm.controls.seller.setValue(response.seller);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: ClientI) {
    this.updateRecord(form);
  }

  updateRecord(form: ClientI) {

    this._clientService.updateClient(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Cliente actualizado');
        this.router.navigate(['seller/clients']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['seller/clients']);
  }

}
