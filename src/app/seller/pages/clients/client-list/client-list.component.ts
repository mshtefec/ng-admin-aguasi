import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { ClientI }                        from '@interfaces/client.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ClientService }                  from '@app/services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'dni', 'email', 'lastname', 'firstname', 'address', 'debe', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private clientService: ClientService,
  ) {
    
  }

  ngOnInit() {
    
    this.clientService.getClients().subscribe( res =>
      this.dataSource.data = res
    );

    let user = JSON.parse(localStorage.getItem('identity'));
    
    this.clientService.getClients().subscribe(res => {
        const filter = res.filter(
          t => t.seller == user.sub
        );
        this.dataSource.data = filter;
      }
    );
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['seller/clients/new']);
  }

  onEdit(data: ClientI) {
    this.router.navigate(['seller/clients/edit/' + data.id]);
  }

  onShow(data: ClientI) {
    this.router.navigate(['seller/clients/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this.clientService.deletedClient(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Cliente borrado.',
              'success'
            )
            this.router.navigate(['seller/clients']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

}
