import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';

import { ClientI }                            from '@interfaces/client.interface';

import { ToastrService }                      from 'ngx-toastr';
import { ClientService }                      from '@services/client.service';
import { UserService }                      from '@services/user.service';

@Component({
  selector: 'app-client-new',
  templateUrl: './client-new.component.html',
  styleUrls: ['./client-new.component.scss']
})
export class ClientNewComponent implements OnInit {

  provinces: any = [
    { "name": "Ciudad Autónoma de Buenos Aires", "country": "AR" },
    { "name": "Buenos Aires", "country": "AR" },
    { "name": "Catamarca", "country": "AR" },
    { "name": "Chaco", "country": "AR" },
    { "name": "Chubut", "country": "AR" },
    { "name": "Córdoba", "country": "AR" },
    { "name": "Corrientes", "country": "AR" },
    { "name": "Entre Ríos", "country": "AR" },
    { "name": "Formosa", "country": "AR" },
    { "name": "Jujuy", "country": "AR" },
    { "name": "La Pampa", "country": "AR" },
    { "name": "La Rioja", "country": "AR" },
    { "name": "Mendoza", "country": "AR" },
    { "name": "Misiones", "country": "AR" },
    { "name": "Neuquén", "country": "AR" },
    { "name": "Río Negro", "country": "AR" },
    { "name": "Salta", "country": "AR" },
    { "name": "San Juan", "country": "AR" },
    { "name": "San Luis", "country": "AR" },
    { "name": "Santa Cruz", "country": "AR" },
    { "name": "Santa Fe", "country": "AR" },
    { "name": "Santiago del Estero", "country": "AR" },
    { "name": "Tierra del Fuego, Antártida e Islas del Atlántico Sur", "country": "AR" },
    { "name": "Tucumán", "country": "AR" }
  ]

  clientForm = new FormGroup({
    lastname: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    dni: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(8)])),
    celphone: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    debe: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    locality: new FormControl('', Validators.required),
    cp: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    seller: new FormControl('', Validators.required),
  });

  constructor(
    private clientService: ClientService,
    private toastr: ToastrService,
    private _userService: UserService,
    public router: Router
  ) { }

  ngOnInit() { 
    let user = JSON.parse(localStorage.getItem('identity'));

    this._userService.getUser(2).subscribe( data => {
      this.clientForm.controls["seller"].setValue(data);
    });
  }

  onSubmit(form: ClientI) {
    this.insertRecord(form);
  }


  insertRecord(form: ClientI) {
    this.clientService.addClient(form).subscribe( 
      response => {
        if (response["status"] == "error") {
          this.toastr.error(response['message']);
        }else if(response["status"] == "success"){
          this.toastr.success('Cambios guardados', 'Cliente añadido');
          
          this.resetForm();
          this.router.navigate(['seller/clients']);
        }         
      },
      error => {
        console.error(error);
      }
    );
  }

  resetForm() {
    this.clientForm.reset();
  }

  onBack() {
    this.router.navigate(['seller/clients']);
  }

}
