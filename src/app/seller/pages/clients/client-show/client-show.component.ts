import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { ClientService }                      from '@services/client.service';

@Component({
  selector: 'app-client-show',
  templateUrl: './client-show.component.html',
  styleUrls: ['./client-show.component.scss']
})
export class ClientShowComponent implements OnInit {

  clientId: number;

  clientForm = new FormGroup({
    id: new FormControl(this.clientId, [Validators.required]),
    lastname: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    dni: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(8)])),
    celphone: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    debe: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    locality: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    cp: new FormControl('', Validators.required),
    seller: new FormControl('', Validators.required),
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private _clientService: ClientService
  ) { }

  

  ngOnInit() {

    this.route.params.subscribe(
      params => {
        this.clientId = +params['id'];

        this._clientService.getClient(this.clientId).subscribe(
          response => {
            this.clientForm.controls.id.setValue(response.id);
            this.clientForm.controls.lastname.setValue(response.lastname);
            this.clientForm.controls.firstname.setValue(response.firstname);
            this.clientForm.controls.dni.setValue(response.dni);
            this.clientForm.controls.celphone.setValue(response.celphone);
            this.clientForm.controls.address.setValue(response.address);
            this.clientForm.controls.email.setValue(response.email);
            this.clientForm.controls.province.setValue(response.province);
            this.clientForm.controls.locality.setValue(response.locality);
            this.clientForm.controls.description.setValue(response.description);
            this.clientForm.controls.cp.setValue(response.cp);
            this.clientForm.controls.debe.setValue(response.debe);
          },
          error => {
            console.error(error);
          }
        );
      }
    );

    
  }

  onBack() {
    this.router.navigate(['seller/clients']);
  }

}
