import { NgModule }                           from '@angular/core';
import { CommonModule }                       from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { NgxMaskModule }                      from 'ngx-mask';
import { ToastrModule }                       from 'ngx-toastr';
import { AutocompleteLibModule }              from 'angular-ng-autocomplete';

import { SellerComponent }                    from './seller.component';
import { SellerRoutingModule }                from './seller-routing.module';

import { OrderListComponent }                 from './pages/orders/order-list/order-list.component';
import { OrderNewComponent }                  from './pages/orders/order-new/order-new.component';
import { ClientListComponent }                from './pages/clients/client-list/client-list.component';
import { ClientNewComponent }                 from './pages/clients/client-new/client-new.component';
import { ClientEditComponent }                from './pages/clients/client-edit/client-edit.component';
import { ClientShowComponent }                from './pages/clients/client-show/client-show.component';
import { SaleListComponent }                  from './pages/sales/sale-list/sale-list.component';

import { NbEvaIconsModule }                   from '@nebular/eva-icons';  

import {
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbMenuModule,
  NbIconModule,
  NbButtonModule,
  NbCardModule,
  NbActionsModule,
  NbInputModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbMediaBreakpoint,
  NbToggleModule,
} from '@nebular/theme';

import { SidebarSellerComponent }             from './sidebar/sidebar-seller.component';

import { MaterialModule }                     from '../thirdmodules/material.module';

@NgModule({
    imports: [
      CommonModule,
      SellerRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      AutocompleteLibModule,

      NbThemeModule.forRoot(),
      NbLayoutModule,
      NbSidebarModule.forRoot(),
      NbMenuModule.forRoot(),
      NbDatepickerModule.forRoot(),
      NbInputModule,
      NbFormFieldModule,
      NbIconModule,
      NbButtonModule,
      NbActionsModule,
      NbCardModule,
      NbEvaIconsModule,
      NbToggleModule,

      NgxMaskModule.forRoot(),
      ToastrModule.forRoot()
    ],
    declarations: [      
      SellerComponent,
      SidebarSellerComponent,
      OrderListComponent,
      OrderNewComponent,
      ClientListComponent,
      ClientNewComponent,
      ClientEditComponent,
      ClientShowComponent,
      SaleListComponent
    ],
    providers: [
      // AuthService,
      // ClientService,
    ],
  })
  export class SellerModule { }