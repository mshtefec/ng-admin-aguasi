import { NgModule } from '@angular/core';

import { AngularFireModule }                            from '@angular/fire';
import { AngularFireDatabaseModule }                    from '@angular/fire/database';
import { AngularFireAuthModule, AngularFireAuth }       from '@angular/fire/auth';
import { AngularFirestoreModule }                       from '@angular/fire/firestore';

import { environment }                                  from '@environments/environment';

@NgModule({
    declarations: [
    ],
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireModule.initializeApp(environment.firebase, "SecondaryApp"),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AngularFirestoreModule,
    ],
    exports: [
    ],
    providers: [
        AngularFireAuth
    ],
})
export class FirebaseModule {}