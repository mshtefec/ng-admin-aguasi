import { NgModule } from '@angular/core';

import { 
    NbThemeModule,
    NbLayoutModule,
    NbSidebarModule,
    NbMenuModule,
    NbContextMenuModule,
    NbIconModule,
    NbButtonModule,
    NbCardModule,
    NbActionsModule,
    NbInputModule,
    NbDatepickerModule,
    NbFormFieldModule,
    NbUserModule,
    NbMediaBreakpoint,
    NbToggleModule, 
  } from '@nebular/theme';
  import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
    imports: [
        NbThemeModule.forRoot(),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbDatepickerModule.forRoot(),
    ],
    exports: [
        NbThemeModule,
        NbSidebarModule,
        NbMenuModule,
        NbDatepickerModule,
        NbContextMenuModule,
        NbLayoutModule,
        NbInputModule,
        NbFormFieldModule,
        NbIconModule,
        NbButtonModule,
        NbActionsModule,
        NbCardModule,
        NbUserModule,
        NbEvaIconsModule,
        NbToggleModule,
    ]
})
export class NebularModule {}