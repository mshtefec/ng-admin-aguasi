// CORE
import { NgModule, LOCALE_ID }                from '@angular/core';
import { FormsModule }                        from '@angular/forms';
import { HttpClientModule, HttpClient }       from '@angular/common/http';
import { registerLocaleData }                 from '@angular/common';
import { BrowserModule }                      from '@angular/platform-browser';
import { BrowserAnimationsModule }            from '@angular/platform-browser/animations';
import { ServiceWorkerModule }                from '@angular/service-worker';

import { environment }                        from '@environments/environment';

import { AppRoutingModule }                   from '@app/app-routing.module';
import { AppComponent }                       from '@app/app.component';

import { AutocompleteLibModule }              from 'angular-ng-autocomplete';

// LOCALE
import localeEs                               from '@angular/common/locales/es';
registerLocaleData(localeEs, 'es-AR');

//Translation
import { TranslateLoader, TranslateModule }   from '@ngx-translate/core';
import { TranslateHttpLoader }                from '@ngx-translate/http-loader';
import { ToastrModule }                       from 'ngx-toastr';

// FIREBASE
import { FirebaseModule }                     from '@thirdmodules/firebase.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // CORE
    FormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
        },
        deps: [ HttpClient ]
      },
    }),

    AppRoutingModule,
    AutocompleteLibModule,
    
    ToastrModule.forRoot(),
    FirebaseModule

  ],
  providers: [
    { provide: LOCALE_ID, useValue: "es-AR" }, //your locale
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
