import { NgModule }                     from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';
import { DeliveryComponent }            from './delivery.component';

import { OrderListComponent }           from './orders/order-list/order-list.component';
import { OrderShowComponent }           from './orders/order-show/order-show.component';

const routes: Routes = [
  { 
    path: '', 
    component: DeliveryComponent,
    children: [
      // { path: 'clients',          component: ClientListComponent, },
      // { path: 'clients/:id',     component: ClientShowComponent, },
      { path: 'orders',           component: OrderListComponent, },
      { path: 'orders/:id',       component: OrderShowComponent, },
      // { path: 'sales',            component: SaleListComponent, },
      // { path: 'sales/:id',            component: SaleListComponent, },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule { }
