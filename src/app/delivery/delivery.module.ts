import { NgModule }                           from '@angular/core';
import { CommonModule }                       from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { NgxMaskModule }                      from 'ngx-mask';
import { ToastrModule }                       from 'ngx-toastr';

import { DeliveryComponent }                  from './delivery.component';
import { DeliveryRoutingModule }              from './delivery-routing.module';

import { OrderListComponent }                 from './orders/order-list/order-list.component';
import { OrderShowComponent }                 from './orders/order-show/order-show.component';

import { NbEvaIconsModule }                   from '@nebular/eva-icons';  

import {
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbMenuModule,
  NbIconModule,
  NbButtonModule,
  NbCardModule,
  NbActionsModule,
  NbInputModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbMediaBreakpoint,
  NbToggleModule,
} from '@nebular/theme';

import { SidebarDeliveryComponent }           from './sidebar/sidebar-delivery.component';

import { MaterialModule }                     from '../thirdmodules/material.module';

@NgModule({
    imports: [
      CommonModule,
      DeliveryRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,

      NbThemeModule.forRoot(),
      NbLayoutModule,
      NbSidebarModule.forRoot(),
      NbMenuModule.forRoot(),
      NbDatepickerModule.forRoot(),
      NbInputModule,
      NbFormFieldModule,
      NbIconModule,
      NbButtonModule,
      NbActionsModule,
      NbCardModule,
      NbEvaIconsModule,
      NbToggleModule,

      NgxMaskModule.forRoot(),
      ToastrModule.forRoot()
    ],
    declarations: [      
      DeliveryComponent,
      SidebarDeliveryComponent,
      OrderListComponent,
      OrderShowComponent
    ],
    providers: [
      // AuthService,
      // ClientService,
    ],
  })
  export class DeliveryModule { }