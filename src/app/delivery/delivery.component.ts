import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { NB_WINDOW, NbMenuService, NbSidebarService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { filter, map } from 'rxjs/operators';

import { MenuTranslatorService } from '@services/menu-translator.service';

@Component({
  selector: 'app-delivery',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
  <nb-layout>

    <nb-layout-header fixed>
      <nb-actions>
        <nb-action icon="menu-outline" (click)="toggle()"></nb-action>
        <nb-action icon="search-outline"></nb-action>
      </nb-actions>
      
    </nb-layout-header>

    <nb-sidebar tag="left">
      <app-sidebar-delivery></app-sidebar-delivery>
    </nb-sidebar>

    <nb-layout-column>
      <router-outlet></router-outlet>
    </nb-layout-column>
  </nb-layout>
  `,
  styles: [`
    :host nb-layout-header ::ng-deep nav {
      justify-content: flex-end;
    }
  `],
  providers: [MenuTranslatorService]
})
export class DeliveryComponent implements OnInit {

  private activeLang = 'es';

  constructor( 
    @Inject(NB_WINDOW) private window,
    private translate: TranslateService,
    private sidebarService: NbSidebarService,
    private nbMenuService: NbMenuService,
    private iconLibraries: NbIconLibraries
  ) {
    this.translate.setDefaultLang(this.activeLang);
    
    this.iconLibraries.registerFontPack('font-awesome', { packClass: 'fa', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('solid', { packClass: 'fas', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('light', {packClass: 'fal', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('duotone', {packClass: 'fad', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('brands', {packClass: 'fab', iconClassPrefix: 'fa'});
  }

  toggle() {
    this.sidebarService.toggle(true, 'left');
  }

  ngOnInit() {
    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe(title => this.window.alert(`${title} was clicked!`));
  }

}
