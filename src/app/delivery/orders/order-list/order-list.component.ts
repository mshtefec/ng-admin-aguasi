import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'client', 'total', 'delivery_date', 'actions'];
  dataSource = new MatTableDataSource();

  countAssigned: number = 0;
  countFinished: number = 0;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _orderService: OrderService,
    public router: Router,
    private toastr: ToastrService
  ) {
    
  }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('identity'));
    
    this._orderService.getOrders().subscribe(res => {
      // Aplico un filtro para los pedidos del usuario, pero trae todos los pedidos.
        const filter = res.filter(
          t => t.delivery.id == user.sub
        );
        this.dataSource.data = filter;

        const assigned = filter.filter(
          o => o.status == 'PENDIENTE'
        );
        const finished = filter.filter(
          o => o.status == 'ENTREGADO' || o.status == 'VENDIDO'
        );
        this.countAssigned = assigned.length;
        this.countFinished = finished.length;
      }
    );
  }

  getTotal() {
    const orders: OrderI[] = this.dataSource.data;
    return orders.map(t => t.total).reduce((acc, value) => acc + value, 0);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onFinish(order: OrderI) {

    Swal.fire({

      title: 'estas seguro que desea marcar como entregado el pedido?',
      text: 'al confirmar usted esta dando su consentimiento y no se puede revertir esta acción!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Finalizar!'

    }).then(result => {

      if (result.value) {

        this._orderService.finishOne(order).subscribe( 
          response => {
            this.toastr.success('Cambios guardados', 'Pedido actualizado');
            this.router.navigate(['delivery/orders']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
  }

  onShow(data: OrderI) {
    this.router.navigate(['delivery/orders/' + data.id]);
  }

  filterAssigned() {
    this.applyFilter('PENDIENTE');
  }

  filterFinished() {
    this.applyFilter('ENTREGADO');
  }

}
