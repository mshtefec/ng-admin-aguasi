import { Injectable }             from '@angular/core';
import { CanActivate, Router }    from '@angular/router';
import { AuthService }            from '@services/auth.service';
import { ToastrService }          from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthSellerGuard implements CanActivate {

  constructor(
    private _authService: AuthService,
    private _router: Router,
    private toastr: ToastrService
  ) { }

  canActivate() {
    let identity = this._authService.getIdentity();
    
    if (identity) {
      if (identity.role == 'ROLE_VENDEDOR') {
        return true;
      } else {
        this.toastr.error("Debe tener privilegios de vendedor para acceder.");
        this._router.navigate(['/login']);
        return false;
      }
    } else {
        this.toastr.error("Debe estar autenticado para acceder.");
        this._router.navigate(['/login']);
        return false;
    }
    
  }
  
}