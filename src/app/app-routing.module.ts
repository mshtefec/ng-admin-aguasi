import { NgModule }                           from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { AuthAdminGuard }                     from './guards/auth-admin.guard';
import { AuthDeliveryGuard }                  from './guards/auth-delivery.guard';
import { AuthSellerGuard }                    from './guards/auth-seller.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { 
    path: '',
    //loadChildren: './auth/auth.module#AuthModule'
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
  { 
    path: 'admin',
    canActivate: [AuthAdminGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
  { 
    path: 'seller',
    canActivate: [AuthSellerGuard],
    loadChildren: () => import('./seller/seller.module').then(m => m.SellerModule),
  },
  { 
    path: 'delivery',
    canActivate: [AuthDeliveryGuard],
    loadChildren: () => import('./delivery/delivery.module').then(m => m.DeliveryModule),
  },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
