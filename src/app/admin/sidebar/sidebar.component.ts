import { Component, OnInit, ChangeDetectionStrategy }       from '@angular/core';
import { NbMenuItem, NbIconLibraries }                      from '@nebular/theme';
import { MenuTranslatorService }                            from '@services/menu-translator.service';
import { AuthService }                                      from '@app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <nb-menu [items]="items" autoCollapse="true">
    </nb-menu>
  `,
  providers: [MenuTranslatorService]
})
export class SidebarComponent implements OnInit {

  private role: string;
  public items: NbMenuItem[];

  constructor(
    private translator: MenuTranslatorService,
    private iconLibraries: NbIconLibraries,
    public auth: AuthService
  ) {
    
    this.iconLibraries.registerFontPack('font-awesome', { packClass: 'fa', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('solid', { packClass: 'fas', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('light', {packClass: 'fal', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('duotone', {packClass: 'fad', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('brands', {packClass: 'fab', iconClassPrefix: 'fa'});
  }

  getDefaultMenu() {
    this.items = [
      {
        title: 'Reportes',
        icon: 'activity-outline',
        link: 'reports'
      },
      {
        title: 'Usuarios',
        icon: 'people-outline',
        link: 'users'
      },
      {
        title: 'Clientes',
        icon: 'people-outline',
        expanded: false,
        children: [
          {
            title: 'Listado',
            icon: 'list-outline',
            link: 'clients'
          },
          {
            title: 'Zonas',
            icon: 'bookmark-outline',
            link: 'zones'
          },
          {
            title: 'Estados',
            icon: 'bookmark-outline',
            link: 'client-states'
          },
          {
            title: 'Origenes',
            icon: 'bookmark-outline',
            link: 'client-origins'
          },
        ],
      },
      {
        title: 'Insumos',
        icon: 'shopping-cart-outline',
        expanded: false,
        children: [
          {
            title: 'Categorias',
            icon: 'bookmark-outline',
            link: 'categories'
          },
          {
            title: 'Articulos',
            icon: 'shopping-cart-outline',
            link: 'supplies'
          },
          {
            title: 'Costos / Gastos',
            icon: 'funnel-outline',
            link: 'expenses'
          },
           {
             title: 'Bidones',
             icon: 'shopping-cart-outline',
             link: 'bottles-list'
           },
        ],
      },
      {
        title: 'Ordenes',
        icon: 'car-outline',
        expanded: false,
        children: [
          {
            title: 'Pedidos',
            icon: 'bookmark-outline',
            link: 'orders'
          },
          {
            title: 'Detalles Pedidos Insumos',
            icon: 'funnel-outline',
            link: 'orders-supplies'
          },
        ],
      },
      {
        title: 'Ventas',
        icon: 'car-outline',
        link: 'sales'
      },
      {
        title: 'Salir',
        icon: 'power-outline',
        link: '../logout/1'
      },
    ];
  }

  getMenuAdmin() {

    this.items = [
      {
        title: 'sidebar.home',
        icon: 'home-outline',
        link: 'admin-dashboard'
      },
      {
        title: 'sidebar.roles',
        icon: 'lock-outline',
        link: 'roles'
      },
      {
        title: 'sidebar.rrhh.title',
        icon: 'person-outline',
        expanded: false,
        children: [
          {
            title: 'sidebar.rrhh.users',
            icon: 'person-outline',
            link: 'users'
          },
          {
            title: 'sidebar.rrhh.packers',
            icon: 'people-outline',
            link: 'users'
          },
          {
            title: 'sidebar.rrhh.traders',
            icon: 'people-outline',
            link: 'users'
          },
          {
            title: 'sidebar.rrhh.deliveries',
            icon: 'people-outline',
            link: 'deliveries',
          },
          {
            title: 'sidebar.rrhh.clients',
            icon: 'people-outline',
            link: 'clients'
          },
        ],
      },
      {
        title: 'sidebar.supplies',
        icon: 'shopping-cart-outline',
        expanded: false,
        children: [
          {
            title: 'sidebar.supplies',
            icon: 'shopping-cart-outline',
            link: 'supplies'
          },
          {
            title: 'Bidones',
            icon: 'shopping-cart-outline',
            link: 'bottles-list'
          },
        ],
      },
      {
        title: 'sidebar.orders',
        icon: 'car-outline',
        link: 'orders'
      },
      {
        title: 'sidebar.sales',
        //icon: { icon: 'dollar-sign', pack: 'font-awesome' }, 
        icon: 'pantone-outline', 
        link: 'sales'
      },
    ];

  }

  getMenuPacker() {

    this.items = [
      {
        title: '',
        icon: 'swap-outline',
        link: '{{ toggleCompact() }}'
      },
      {
        title: 'Inicio',
        icon: 'home-outline',
        link: 'packer-dashboard'
      },
      {
        title: 'Clientes',
        icon: 'people-outline',
        link: 'clients'
      },
      {
        title: 'Insumos',
        icon: 'shopping-cart-outline',
        link: 'supplies'
      },
      {
        title: 'Pedidos',
        icon: 'car-outline',
        link: 'orders'
      },
      {
        title: 'Salir',
        icon: 'power-outline',
      },
    ];

  }

  getMenuDelivery() {

    this.items = [
      {
        title: '',
        icon: 'swap-outline',
        link: '{{ toggleCompact() }}'
      },
      {
        title: 'Inicio',
        icon: 'home-outline',
        link: 'delivery-dashboard'
      },
      {
        title: 'Clientes',
        icon: 'people-outline',
        link: 'clients'
      },
      {
        title: 'Insumos',
        icon: 'shopping-cart-outline',
        link: 'supplies'
      },
      {
        title: 'Pedidos',
        icon: 'car-outline',
        link: 'orders'
      },
      {
        title: 'Salir',
        icon: 'power-outline',
      },
    ];

  }

  ngOnInit() {
    this.settingMenu();
    this.items = this.translator.translate(this.items);
  }

  settingMenu() {
    switch(this.role) { 
      case 'envasador': { 
        this.getMenuPacker();
        break; 
      } 
      case 'repartidor': { 
        this.getMenuDelivery();
        break; 
      } 
      case 'editor': { 
        this.getDefaultMenu();
        break; 
      } 
      default: { 
        //this.getMenuAdmin();
        this.getDefaultMenu();
        break; 
      } 
    } 
  }

}
