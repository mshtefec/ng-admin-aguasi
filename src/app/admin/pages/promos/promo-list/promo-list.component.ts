import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { CrudService }                    from '@services/crud.service';
import { PromoI }                         from '@interfaces/promo.interface';

//import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';

@Component({
  selector: 'app-promo-list',
  templateUrl: './promo-list.component.html',
  styleUrls: ['./promo-list.component.scss']
})
export class PromoListComponent implements OnInit {

  flipped: boolean = false;

  toggle() {
    this.flipped = !this.flipped;
  }

  displayedColumns: string[] = ['id', 'description', 'total', 'active', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private crudSvc: CrudService,
    public router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.crudSvc.initConfig('promos');

    this.crudSvc.getAllWithId().subscribe(res => 
      this.dataSource.data = res
    );
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    window.localStorage.removeItem("promoId");
    this.router.navigate(['admin/promos/form']);
  }

  onEdit(data: PromoI) {
    window.localStorage.removeItem("promoId");
    window.localStorage.setItem("promoId", data.id.toString());
    this.router.navigate(['admin/promos/form']);
  }

  onShow(data: PromoI) {
    window.localStorage.removeItem("promoId");
    window.localStorage.setItem("promoId", data.id.toString());
    this.router.navigate(['admin/promos/show']);
  }

  onDeleted(data: PromoI) {
    // Swal.fire({
    //   title: 'estas seguro?',
    //   text: 'al eliminar este elemento no se puede revertir!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Si, eliminar!'

    // }).then(result => {

    //   if (result.value) {
        
        this.crudSvc.deletedObject(data).then(() => {
          this.toastr.success('Eliminado!', 'Elemento eliminado correctamente');
        }).catch((error) => {
          this.toastr.error('Error!', 'No se puede eliminar este elemento');
        });

    //   }
    // });
  }

}
