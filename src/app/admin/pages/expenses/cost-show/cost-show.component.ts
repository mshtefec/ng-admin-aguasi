import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { SupplyService }                      from '@app/services/supply.service';

@Component({
  selector: 'app-cost-show',
  templateUrl: './cost-show.component.html',
  styleUrls: ['./cost-show.component.scss']
})
export class CostShowComponent implements OnInit {

  supplyId: number;

  supplyForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    category: new FormControl('', [Validators.required, Validators.minLength(3)]),
    price : new FormControl('', Validators.required),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private _supplyService: SupplyService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.supplyId = +params['id'];

        this._supplyService.getSupply(this.supplyId).subscribe(
          response => {
            this.supplyForm.controls.name.setValue(response.name);
            this.supplyForm.controls.description.setValue(response.description);
            this.supplyForm.controls.category.setValue(response.category.name);
            this.supplyForm.controls.price.setValue('$ ' + Number(response.price).toFixed());
            this.supplyForm.controls.active.setValue(Number(response.active));
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/expenses']);
  }

}
