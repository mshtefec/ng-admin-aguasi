import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';

import { SupplyI }                            from '../../../../interfaces/supply.interface';

import { ToastrService }                      from 'ngx-toastr';
import { SupplyService }                      from '@app/services/supply.service';
import { CategoryExpService }                 from '@app/services/categoryexp.service';
import { CategoryExpI }                       from '@app/interfaces/categoryexp.interface';

@Component({
  selector: 'app-cost-new',
  templateUrl: './cost-new.component.html',
  styleUrls: ['./cost-new.component.scss']
})
export class CostNewComponent implements OnInit {

  supplyId: string;
  categories: CategoryExpI[];

  supplyForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    category: new FormControl('', [Validators.required, Validators.minLength(3)]),
    stock: new FormControl('0', Validators.required),
    type: new FormControl('1',[Validators.min(0), Validators.required]),
    price : new FormControl('', [Validators.required, Validators.min(0), Validators.max(999999)]),
    created_at: new FormControl('', Validators.required),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private _supplyService: SupplyService,
    private _categoryExpService: CategoryExpService
  ) { }

  ngOnInit() { 
    this.supplyForm.controls.type.setValue(Number(1));
    this.supplyForm.controls.stock.setValue("0.00");
    this._categoryExpService.getCategoriesExp().subscribe( data => {
      this.categories = data;
    });
  }

  onSubmit(form: SupplyI) {
    this.insertRecord(form);
  }

  insertRecord(form: SupplyI) {

    this._supplyService.addSupply(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Costo añadido');
        this.router.navigate(['admin/expenses']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/expenses']);
  }

}
