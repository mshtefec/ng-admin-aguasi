import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }                             from '@angular/router';

import { SupplyI }                            from '../../../../interfaces/supply.interface';

import { ToastrService }                      from 'ngx-toastr';
import { SupplyService }                      from '@app/services/supply.service';
import { CategoryExpService }                 from '@app/services/categoryexp.service';
import { CategoryExpI }                       from '@app/interfaces/categoryexp.interface';

@Component({
  selector: 'app-cost-edit',
  templateUrl: './cost-edit.component.html',
  styleUrls: ['./cost-edit.component.scss']
})
export class CostEditComponent implements OnInit {

  supplyId: number;
  categories: CategoryExpI[];

  supplyForm = new FormGroup({
    id: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    category: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    stock: new FormControl('0', Validators.required),
    type: new FormControl('1',[Validators.min(0), Validators.required]),
    price : new FormControl('', [Validators.required, Validators.min(0), Validators.max(999999)]),
    created_at: new FormControl('', Validators.required),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _supplyService: SupplyService,
    private _categoryExpService: CategoryExpService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.supplyId = +params['id'];

        this._supplyService.getSupply(this.supplyId).subscribe(
          response => {
            this.supplyForm.controls.id.setValue(response.id);
            this.supplyForm.controls.name.setValue(response.name);
            this.supplyForm.controls.description.setValue(response.description);
            this.supplyForm.controls.stock.setValue(Number(response.stock));
            this.supplyForm.controls.type.setValue(Number(1));
            this.supplyForm.controls.price.setValue(Number(response.price));
            this.supplyForm.controls.created_at.setValue(response.created_at);
            this.supplyForm.controls.active.setValue(Number(response.active));
          },
          error => {
            console.error(error);
          }
        );
      }
    );

    this._categoryExpService.getCategoriesExp().subscribe( data => {
      this.categories = data;
    });
  }

  onSubmit(form: SupplyI) {
    this.updateRecord(form);
  }

  updateRecord(form: SupplyI) {

    this._supplyService.updateSupply(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Costo actualizado');
        this.router.navigate(['admin/expenses']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/expenses']);
  }

}
