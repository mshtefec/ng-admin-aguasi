import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';

import { CategoryExpI }                       from '../../../../../interfaces/categoryexp.interface';

import { ToastrService }                      from 'ngx-toastr';
import { CategoryExpService }                 from '@app/services/categoryexp.service';

@Component({
  selector: 'app-categoryexp-new',
  templateUrl: './categoryexp-new.component.html',
  styleUrls: ['./categoryexp-new.component.scss']
})
export class CategoryExpNewComponent implements OnInit {

  categoryexpId: string;

  categoryExpForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private _categoryexpService: CategoryExpService
  ) { }

  ngOnInit() {
  }

  onSubmit(form: CategoryExpI) {
    this.insertRecord(form);
  }

  insertRecord(form: CategoryExpI) {

    this._categoryexpService.addCategoryExp(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Categoria añadida');
        this.router.navigate(['admin/categories']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/categories']);
  }

}
