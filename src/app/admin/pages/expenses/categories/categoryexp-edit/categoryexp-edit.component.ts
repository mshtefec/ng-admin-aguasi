import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { CategoryExpI }                       from '../../../../../interfaces/categoryexp.interface';

import { ToastrService }                      from 'ngx-toastr';
import { CategoryExpService }                 from '@app/services/categoryexp.service';

@Component({
  selector: 'app-categoryexp-edit',
  templateUrl: './categoryexp-edit.component.html',
  styleUrls: ['./categoryexp-edit.component.scss']
})
export class CategoryExpEditComponent implements OnInit {

  categoryExpId: number;

  categoryExpForm = new FormGroup({
    id: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _categoryExpService: CategoryExpService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.categoryExpId = +params['id'];

        this._categoryExpService.getCategoryExp(this.categoryExpId).subscribe(
          response => {
            this.categoryExpForm.controls.id.setValue(response.id);
            this.categoryExpForm.controls.name.setValue(response.name);
            this.categoryExpForm.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: CategoryExpI) {
    this.updateRecord(form);
  }

  updateRecord(form: CategoryExpI) {

    this._categoryExpService.updateCategoryExp(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Categoria actualizada');
        this.router.navigate(['admin/categories']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/categories']);
  }

}
