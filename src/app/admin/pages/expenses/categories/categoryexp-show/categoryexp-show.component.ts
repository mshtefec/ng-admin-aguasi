import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { CategoryExpService }                 from '@app/services/categoryexp.service';

@Component({
  selector: 'app-categoryexp-show',
  templateUrl: './categoryexp-show.component.html',
  styleUrls: ['./categoryexp-show.component.scss']
})
export class CategoryExpShowComponent implements OnInit {

  categoryexpId: number;

  categoryExpForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private _categoryExpService: CategoryExpService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.categoryexpId = +params['id'];

        this._categoryExpService.getCategoryExp(this.categoryexpId).subscribe(
          response => {
            this.categoryExpForm.controls.name.setValue(response.name);
            this.categoryExpForm.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/categories']);
  }

}
