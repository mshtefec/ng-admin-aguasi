import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import { CategoryExpI }                   from '../../../../../interfaces/categoryexp.interface';

import Swal                               from 'sweetalert2';
import { MatDialog }                      from '@angular/material/dialog';
import { ToastrService }                  from 'ngx-toastr';
import { CategoryExpService }             from '@app/services/categoryexp.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-categoryexp-list',
  templateUrl: './categoryexp-list.component.html',
  styleUrls: ['./categoryexp-list.component.scss']
})
export class CategoryExpListComponent implements OnInit {

  flipped: boolean = false;

  toggle() {
    this.flipped = !this.flipped;
  }

  displayedColumns: string[] = ['name', 'description', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private excelService: ExporterService,
    private _categoryexpService: CategoryExpService
  ) { }

  ngOnInit() {
    this._categoryexpService.getCategoriesExp().subscribe(res => {
      this.dataSource.data = res;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/categories/new']);
  }

  onEdit(data: CategoryExpI) {
    this.router.navigate(['admin/categories/edit/' + data.id]);
  }

  onShow(data: CategoryExpI) {
    this.router.navigate(['admin/categories/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._categoryexpService.deletedCategoryExp(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Usuario borrado.',
              'success'
            )
            this.router.navigate(['admin/categories']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_categorias_costos');
  }

}
