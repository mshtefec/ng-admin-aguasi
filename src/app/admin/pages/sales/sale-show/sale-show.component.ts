import { Component, OnInit }                  from '@angular/core';
import { DatePipe }                           from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { OrderService }                      from '@services/order.service';

@Component({
  selector: 'app-sale-show',
  templateUrl: './sale-show.component.html',
  styleUrls: ['./sale-show.component.scss'],
})
export class SaleShowComponent implements OnInit {

  orderId: number;

  orderForm = new FormGroup({
    clientName: new FormControl('', Validators.required),
    clientTel: new FormControl('', Validators.required),
    clientDir: new FormControl('', Validators.required),
    sellerName: new FormControl('', Validators.required),
    sellerUser: new FormControl('', Validators.required),
    sellerEmail: new FormControl('', Validators.required),
    deliveryName: new FormControl('', Validators.required),
    deliveryUser: new FormControl('', Validators.required),
    deliveryEmail: new FormControl('', Validators.required),
    supplyName: new FormControl('', Validators.required),
    supplyDesc: new FormControl('', Validators.required),
    supplyPrice: new FormControl('', Validators.required),
    supplyQty: new FormControl('', Validators.required),
    total: new FormControl('', Validators.required),
    deliveryDate: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private _orderService: OrderService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.orderId = +params['id'];

        this._orderService.getOrder(this.orderId).subscribe(
          response => {
            this.orderForm.controls.clientName.setValue(response.client.lastname + ', ' + response.client.firstname);
            this.orderForm.controls.clientTel.setValue(response.client.celphone);
            this.orderForm.controls.clientDir.setValue(response.client.address);
            this.orderForm.controls.sellerName.setValue(response.seller.lastname + ', ' + response.seller.firstname);
            this.orderForm.controls.sellerUser.setValue(response.seller.username);
            this.orderForm.controls.sellerEmail.setValue(response.seller.email);
            this.orderForm.controls.deliveryName.setValue(response.delivery.lastname + ', ' + response.delivery.firstname);
            this.orderForm.controls.deliveryUser.setValue(response.delivery.username);
            this.orderForm.controls.deliveryEmail.setValue(response.delivery.email);
            this.orderForm.controls.supplyName.setValue(response.supply.name);
            this.orderForm.controls.supplyDesc.setValue(response.supply.description);
            this.orderForm.controls.supplyPrice.setValue('$ ' + Number(response.supply.price).toFixed(2));
            this.orderForm.controls.supplyQty.setValue(response.quantity);
            this.orderForm.controls.total.setValue('$ ' + Number(response.total).toFixed(2));
            this.orderForm.controls.deliveryDate.setValue(this.datePipe.transform(response.delivery_date.date, 'dd/MM/yyyy'));
            this.orderForm.controls.description.setValue(response.description);
            this.orderForm.controls.status.setValue(response.status);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/sales']);
  }

}
