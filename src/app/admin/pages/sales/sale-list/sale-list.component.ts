import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

//import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.scss']
})
export class SaleListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'client', 'total', 'delivery_date', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private excelService: ExporterService,
    private _orderService: OrderService,
    public router: Router,
    private toastr: ToastrService
  ) {
    
  }

  ngOnInit() {
    
    this._orderService.getOrders().subscribe(res => {
      // Aplico un filtro para los pedidos vendidos.
        const filter: any = res.filter(
          t => t.status == 'VENDIDO'
        );

        filter.forEach(m => {
          m.client = m.client.lastname + ', ' + m.client.firstname;
          m.seller = m.seller.lastname + ', ' + m.seller.firstname;
          m.delivery = m.delivery.lastname + ', ' + m.delivery.firstname;
          m.supply = m.supply.name;
          m.delivery_date = m.delivery_date.date,
          m.created_at = m.created_at.date,
          m.updated_at = m.updated_at.date
        });

        this.dataSource.data = filter;
      }
    );
  }

  onShow(data: OrderI) {
    this.router.navigate(['admin/sales/' + data.id]);
  }

  getTotal() {
    const orders: OrderI[] = this.dataSource.data;
    return orders.map(t => t.total).reduce((acc, value) => Number(acc) + Number(value), 0);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_ventas');
  }

}
