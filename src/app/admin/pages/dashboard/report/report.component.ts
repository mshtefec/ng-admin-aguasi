import { Component, OnInit }              from '@angular/core';
import { Router }                         from '@angular/router';

import { ToastrService }                  from 'ngx-toastr';

import { UserService }                    from '@app/services/user.service';
import { ClientService }                  from '@app/services/client.service';
import { SupplyService }                  from '@app/services/supply.service';
import { OrderService }                   from '@app/services/order.service';

import { Observable }                     from 'rxjs';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  public usrCounts: Number = 0;
  public admCounts: Number = 0;
  public delCounts: Number = 0;
  public selCounts: Number = 0;
  public cliCounts: Number = 0;
  public artCounts: Number = 0;
  public ordCounts: Number = 0;
  public penCounts: Number = 0;
  public disCounts: Number = 0;
  public entCounts: Number = 0;
  public venCounts: Number = 0;
  public expCounts: Number = 0;
  public expCost: String;
  public venCost: String;

  public colorStyle: any;
  
  public pColor: string;
  public pSize: number;

  updateColorCard(color: string) {
    this.pColor = color;
    this.setStyle();
  }

  onInputChange(event: any) {
    this.pSize = event.value;
    this.setStyle();
  }

  setStyle() {
    this.colorStyle = {
      'color': this.pColor ? this.pColor : 'primary',
      'font-size': this.pSize ? this.pSize + 'px' : '14px'
    }
  }

  constructor(
    public router: Router,
    private toastr: ToastrService,
    private _userService: UserService,
    private _clientService: ClientService,
    private _supplyService: SupplyService,
    private _orderService: OrderService
  ) { }

  ngOnInit() {
    this._userService.getUsers().subscribe( res => {
      const admins = res.filter(
        u => u.role == 'ROLE_ADMIN'
      );
      const deliveries = res.filter(
        u => u.role == 'ROLE_REPARTIDOR'
      );
      const sellers = res.filter(
        u => u.role == 'ROLE_VENDEDOR'
      );
      this.usrCounts = res.length;
      this.admCounts = admins.length;
      this.delCounts = deliveries.length;
      this.selCounts = sellers.length;
    });
    this._clientService.getClients().subscribe( res => this.cliCounts = res.length );
    this._supplyService.getSupplies().subscribe(res => {
      const filter = res.filter(
        t => t.type == 0
      );
      this.artCounts = filter.length;
    });
    this._supplyService.getSupplies().subscribe(res => {
      const filter = res.filter(
        t => t.type == 1
      );
      this.expCounts = filter.length;
      const tot = filter.map(t => t.price).reduce((acc, value) => Number(acc) + Number(value), 0);
      this.expCost = new Intl.NumberFormat('es-US', {currency: 'USD', style: 'currency'}).format(tot);
    });
    this._orderService.getOrders().subscribe( res => {
      const pendientes = res.filter(
        o => o.status == 'PENDIENTE'
      );
      const distribucion = res.filter(
        o => o.status == 'DISTRIBUCION'
      );
      const entregados = res.filter(
        o => o.status == 'ENTREGADO'
      );
      const vendidos = res.filter(
        o => o.status == 'VENDIDO'
      );

      const totalVendidos = vendidos.map(t => t.total).reduce((acc, value) => Number(acc) + Number(value), 0);

      this.ordCounts = res.length;
      this.penCounts = pendientes.length;
      this.disCounts = distribucion.length;
      this.entCounts = entregados.length;
      this.venCounts = vendidos.length;
      this.venCost = new Intl.NumberFormat('es-US', {currency: 'USD', style: 'currency'}).format(totalVendidos);
    });
  }

}
