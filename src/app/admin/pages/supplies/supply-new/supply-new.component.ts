import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';

import { CrudService }                        from '../../../../services/crud.service';

import { SupplyI }                            from '../../../../interfaces/supply.interface';

import { ToastrService }                      from 'ngx-toastr';
import { SupplyService } from '@app/services/supply.service';

@Component({
  selector: 'app-supply-new',
  templateUrl: './supply-new.component.html',
  styleUrls: ['./supply-new.component.scss']
})
export class SupplyNewComponent implements OnInit {

  supplyId: string;

  supplyForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    stock: new FormControl('0',[Validators.min(0), Validators.required]),
    type: new FormControl('', Validators.required),
    category: new FormControl(null),
    price : new FormControl('0', [Validators.required, Validators.min(0), Validators.max(999999)]),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    private crudSvc: CrudService,
    private toastr: ToastrService,
    public router: Router,
    private _supplyService: SupplyService
  ) { }

  ngOnInit() {
  }

  onSubmit(form: SupplyI) {
    form.type = Number(0);
    this.insertRecord(form);
  }

  insertRecord(form: SupplyI) {

    this._supplyService.addSupply(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Insumo añadido');
        this.router.navigate(['admin/supplies']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/supplies']);
  }

}
