import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import { CrudService }                    from '../../../../services/crud.service';
import { SupplyI }                        from '../../../../interfaces/supply.interface';

import Swal                               from 'sweetalert2';
import { MatDialog }                      from '@angular/material/dialog';
import { ToastrService }                  from 'ngx-toastr';
import { SupplyService }                  from '@app/services/supply.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-supply-list',
  templateUrl: './supply-list.component.html',
  styleUrls: ['./supply-list.component.scss']
})
export class SupplyListComponent implements OnInit {

  flipped: boolean = false;

  toggle() {
    this.flipped = !this.flipped;
  }

  displayedColumns: string[] = ['name', 'description', 'stock', 'price', 'active', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private crudSvc: CrudService,
    public router: Router,
    private toastr: ToastrService,
    private excelService: ExporterService,
    private _supplyService: SupplyService
  ) { }

  ngOnInit() {
    this.crudSvc.initConfig('supplies');

    this._supplyService.getSupplies().subscribe(res => {
      const filter = res.filter(
        t => t.type == 0
      );
      const dateMaping: any = filter;

      dateMaping.forEach(m => {
        m.created_at = m.created_at.date,
        m.updated_at = m.updated_at.date
      });

      this.dataSource.data = dateMaping;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/supplies/new']);
  }

  onEdit(data: SupplyI) {
    this.router.navigate(['admin/supplies/edit/' + data.id]);
  }

  onShow(data: SupplyI) {
    this.router.navigate(['admin/supplies/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._supplyService.deletedSupply(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Usuario borrado.',
              'success'
            )
            this.router.navigate(['admin/supplies']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_insumos');
  }

}
