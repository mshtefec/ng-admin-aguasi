import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

//import { CrudService } from '@services/crud.service';
import { SupplyService }                  from '@app/services/supply.service';
import { ExporterService }                from '@app/services/exporter.service';
import { NbDialogService }                from '@nebular/theme';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';

import { BottledFormComponent }           from '../bottled-form/bottled-form.component';

@Component({
  selector: 'app-bottled-list',
  templateUrl: './bottled-list.component.html',
  styleUrls: ['./bottled-list.component.scss'],
})
export class BottledListComponent implements OnInit {
  flipped: boolean = false;

  toggle() {
    this.flipped = !this.flipped;
  }

  displayedColumns: string[] = ['name', 'description', 'stock', 'price', 'active', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _supplyService: SupplyService,
    private dialogService: NbDialogService,
    public router: Router,
    private excelService: ExporterService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._supplyService.getSupplies().subscribe(res => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      const filter = res.filter(
        t => t.type == 2
      );
      this.dataSource.data = filter;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.dialogService.open(BottledFormComponent, {
      context: {
        title: 'Nuevo bidón',
      },
    });
  }

  onEdit(data: any) {
    this.dialogService.open(BottledFormComponent, {
      context: {
        title: 'Editando bidón',
        bottle: data
      },
    });
  }

  onShow(data: any) {
    this.dialogService.open(BottledFormComponent, {
      context: {
        title: 'Detalles del bidón',
        bottle: data,
        isReadonly: true
      },
    });
  }

  onDeleted(id: number) {
     Swal.fire({
       title: 'estas seguro?',
       text: 'al eliminar este elemento no se puede revertir!',
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Si, eliminar!'

     }).then(result => {

      if (result.value) {

        this._supplyService.deletedSupply(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Bidón borrado.',
              'success'
            )
            this.router.navigate(['admin/bottles-list']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_bidones');
  }

}
