import { Component, OnInit }                  from '@angular/core';
import { Router }                             from '@angular/router';
import { ToastrService }                      from 'ngx-toastr';
import { ApiRestService }                     from '@app/services/api-rest.service';


@Component({
  selector: 'app-bottled-form',
  templateUrl: './bottled-form.component.html',
  styleUrls: ['./bottled-form.component.scss'],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'supplies' },
  ]
})
export class BottledFormComponent implements OnInit {

  bottle: any;
  title: string ;
  isNew: Boolean = false;
  isReadonly: Boolean = false;

  constructor(
    private _api: ApiRestService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    if (!this.bottle) {
      this.isNew = true;
      this.bottle = { 
        type: 2
      };
    }
  }

  onSubmit(form: any) {
    if(this.isNew) {
      form.type = 2;
      this.newRecord(form);
    } else {
      form.type = 2;
      this.updateRecord(form);
    }
  }

  newRecord(form: any) {
    this._api.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Bidón añadido');
        this.router.navigate(['admin/bottles-list']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }

  updateRecord(form: any) {
    this._api.update(form).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Bidón Añadido');
        this.router.navigate(['admin/bottles-list']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/bottles-list']);
  }

}
