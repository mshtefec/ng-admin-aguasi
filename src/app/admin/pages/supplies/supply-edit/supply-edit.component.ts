import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }                             from '@angular/router';

import { SupplyI }                            from '../../../../interfaces/supply.interface';

import { ToastrService }                      from 'ngx-toastr';
import { SupplyService } from '@app/services/supply.service';

@Component({
  selector: 'app-supply-edit',
  templateUrl: './supply-edit.component.html',
  styleUrls: ['./supply-edit.component.scss']
})
export class SupplyEditComponent implements OnInit {

  supplyId: number;

  supplyForm = new FormGroup({
    id: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    stock: new FormControl('0',[Validators.min(0), Validators.required]),
    type: new FormControl('0',[Validators.min(0), Validators.required]),
    price : new FormControl('0', [Validators.required, Validators.min(0), Validators.max(999999)]),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _supplyService: SupplyService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.supplyId = +params['id'];

        this._supplyService.getSupply(this.supplyId).subscribe(
          response => {
            this.supplyForm.controls.id.setValue(response.id);
            this.supplyForm.controls.name.setValue(response.name);
            this.supplyForm.controls.description.setValue(response.description);
            this.supplyForm.controls.stock.setValue(Number(response.stock));
            this.supplyForm.controls.price.setValue(Number(response.price));
            this.supplyForm.controls.type.setValue(Number(response.type));
            this.supplyForm.controls.active.setValue(Number(response.active));
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: SupplyI) {
    this.updateRecord(form);
  }

  updateRecord(form: SupplyI) {

    this._supplyService.updateSupply(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Insumo actualizado');
        this.router.navigate(['admin/supplies']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/supplies']);
  }

}
