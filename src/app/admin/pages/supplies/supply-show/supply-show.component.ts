import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { SupplyService }                      from '@app/services/supply.service';

@Component({
  selector: 'app-supply-show',
  templateUrl: './supply-show.component.html',
  styleUrls: ['./supply-show.component.scss']
})
export class SupplyShowComponent implements OnInit {

  supplyId: number;

  supplyForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3)]),
    stock: new FormControl('0',[Validators.required]),
    price : new FormControl('', Validators.required),
    active: new FormControl(false, [Validators.required])
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private _supplyService: SupplyService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.supplyId = +params['id'];

        this._supplyService.getSupply(this.supplyId).subscribe(
          response => {
            this.supplyForm.controls.name.setValue(response.name);
            this.supplyForm.controls.description.setValue(response.description);
            this.supplyForm.controls.stock.setValue(Number(response.stock));
            this.supplyForm.controls.price.setValue('$ ' + Number(response.price).toFixed());
            this.supplyForm.controls.active.setValue(Number(response.active));
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/supplies']);
  }

}
