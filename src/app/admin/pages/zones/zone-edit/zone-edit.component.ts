import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { ZoneI }                              from '../../../../interfaces/zone.interface';

import { ToastrService }                      from 'ngx-toastr';
import { ZoneService }                        from '@app/services/zone.service';

@Component({
  selector: 'app-zone-edit',
  templateUrl: './zone-edit.component.html',
  styleUrls: ['./zone-edit.component.scss']
})
export class ZoneEditComponent implements OnInit {

  id: number;

  form = new FormGroup({
    id: new FormControl(this.id, [Validators.required]),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _service: ZoneService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.id = +params['id'];

        this._service.getOne(this.id).subscribe(
          response => {
            this.form.controls.id.setValue(response.id);
            this.form.controls.name.setValue(response.name);
            this.form.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: ZoneI) {
    this.updateRecord(form);
  }

  updateRecord(form: ZoneI) {

    this._service.update(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Elemento actualizado');
        this.router.navigate(['admin/zones']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/zones']);
  }

}
