import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatDialog }                      from '@angular/material/dialog';
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import Swal                               from 'sweetalert2';

import { ZoneI }                          from '@interfaces/zone.interface';

import { ZoneService }                    from '@app/services/zone.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.scss']
})
export class ZoneListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'description', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private excelService: ExporterService,
    private _service: ZoneService
  ) { }

  ngOnInit() {
    
    this._service.getAll().subscribe(res => {
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/zones/new']);
  }

  onShow(data: ZoneI) {
    this.router.navigate(['admin/zones/' + data.id]);
  }

  onEdit(data: ZoneI) {
    this.router.navigate(['admin/zones/edit/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._service.deleted(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Elemento borrado.',
              'success'
            )
            this.router.navigate(['admin/zones']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_zonas');
  }

}
