import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';
import { ZoneI }                              from '@interfaces/zone.interface';
import { ZoneService }                        from '@app/services/zone.service';
import { ToastrService }                      from 'ngx-toastr';

@Component({
  selector: 'app-zone-new',
  templateUrl: './zone-new.component.html',
  styleUrls: ['./zone-new.component.scss']
})
export class ZoneNewComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private _service: ZoneService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() { }

  onSubmit(form: ZoneI) {
    this.insertRecord(form);
  }


  insertRecord(form: ZoneI) {
    this._service.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Elemento añadido');
        this.resetForm();
        this.router.navigate(['admin/zones']);
      },
      error => {
        console.error(error);
      }
    );
  }

  resetForm() {
    this.form.reset();
  }

  onBack() {
    this.router.navigate(['admin/zones']);
  }

}
