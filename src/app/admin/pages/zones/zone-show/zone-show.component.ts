import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { ToastrService }                      from 'ngx-toastr';
import { ZoneService }                        from '@app/services/zone.service';

@Component({
  selector: 'app-zone-show',
  templateUrl: './zone-show.component.html',
  styleUrls: ['./zone-show.component.scss']
})
export class ZoneShowComponent implements OnInit {

  id: number;

  form = new FormGroup({
    id: new FormControl(this.id, [Validators.required]),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _service: ZoneService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.id = +params['id'];

        this._service.getOne(this.id).subscribe(
          response => {
            this.form.controls.id.setValue(response.id);
            this.form.controls.name.setValue(response.name);
            this.form.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/zones']);
  }

}
