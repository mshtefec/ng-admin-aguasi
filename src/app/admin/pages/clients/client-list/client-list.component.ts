import { Component, ViewChild, OnInit  }  from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import Swal                               from 'sweetalert2';

import { ClientFormComponent }            from '@app/admin/pages/clients/client-form/client-form.component';

import { ExporterService }                from '@app/services/exporter.service';
import { NbDialogService }                from '@nebular/theme';
import { ApiRestService }                 from '@services/api-rest.service';
import { ClientService }                  from '@app/services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'clients' },
  ]
})
export class ClientListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'dni', 'name', 'lastname','debe', 'province', 'locality', 'cp', 'zone', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private excelService: ExporterService,
    private dialogService: NbDialogService,
    private _api: ApiRestService,
    private _clients: ClientService
  ) { }
  
  ngOnInit() {
    
    this._clients.getClients().subscribe( res => {
      this.dataSource.data = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
     
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.dialogService.open(ClientFormComponent, {
      context: {
        title: 'Nuevo cliente',
      },
    });
  }

  onEdit(element: any) {
    this.dialogService.open(ClientFormComponent, {
      context: {
        title: 'Editando cliente',
        client: element
      },
    });
  }

  onShow(element: any) {
    this.dialogService.open(ClientFormComponent, {
      context: {
        title: 'Detalles del cliente',
        client: element,
        isReadonly: true
      },
    });
  }

  onHistory(id: number) {
    this.router.navigate(['admin/client-history/' + id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._api.deleted(id).subscribe(
          response => {
            //this.toastr.warning('Cambios guardados', 'Cliente borrado');
            Swal.fire(
              'Cambios guardados!',
              'Cliente borrado.',
              'success'
            )
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {

    this._api.getAll().subscribe( response => {

      let excel: clientExcelColumns[] = [];
      
      response.forEach(c => {
        const element: clientExcelColumns = {
          id: c.id,
          apellido: c.lastname,
          nombre: c.firstname,
          dni: c.dni,
          telefono: c.celphone,
          direccion: c.address,
          email: c.email,
          debe: c.debe,
          provincia: c.province,
          localidad: c.locality,
          codigo_postal: c.cp,
          descripcion: c.description,
          vendedor: c.seller.id + ' - ' + c.seller.lastname + ', ' + c.seller.firstname,
          zona: c.zone.name,
          fecha_alta: c.high_date.date,
          estado: c.status.name,
          origen: c.origin.name,
          creado: c.created_at.date,
          actualizado: c.updated_at.date
        };

        if (c.orders.length > 0) {
          let ordersClient: Array<string> = [];
          c.orders.forEach(o => {
            ordersClient.push(o.id);
          });
          element.pedidos = ordersClient.join(', ');
        } else {
          element.pedidos = 'no tiene pedidos';
        }

        excel.push(element);
      });

      this.excelService.exportToExcel(excel, 'listado_de_clientes');

    });
  }

}

export interface clientExcelColumns {
  id?: string;
  apellido?: string;
  nombre?: string;
  dni?: string;
  telefono?: string;
  direccion?: string;
  email?: string;
  debe?: string;
  provincia?: string;
  localidad?: string;
  codigo_postal?: string;
  descripcion?: string;
  vendedor?: string;
  zona?: string;
  fecha_alta?: string;
  estado?: string;
  origen?: string;
  pedidos?: string;
  creado?: string;
  actualizado?: string;
}
