import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';
import { ClientOriginI }                      from '@app/interfaces/client-origin.interface';
import { ClientOriginService }                from '@app/services/client-origin.service';
import { ToastrService }                      from 'ngx-toastr';

@Component({
  selector: 'app-origin-new',
  templateUrl: './origin-new.component.html',
  styleUrls: ['./origin-new.component.scss']
})
export class ClientOriginNewComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private _service: ClientOriginService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() { }

  onSubmit(form: ClientOriginI) {
    this.insertRecord(form);
  }


  insertRecord(form: ClientOriginI) {
    this._service.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Elemento añadido');
        this.resetForm();
        this.router.navigate(['admin/client-origins']);
      },
      error => {
        console.error(error);
      }
    );
  }

  resetForm() {
    this.form.reset();
  }

  onBack() {
    this.router.navigate(['admin/client-origins']);
  }

}
