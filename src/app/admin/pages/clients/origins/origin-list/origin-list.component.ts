import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatDialog }                      from '@angular/material/dialog';
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import Swal                               from 'sweetalert2';

import { ClientOriginI }                  from '@app/interfaces/client-origin.interface';

import { ClientOriginService }            from '@app/services/client-origin.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-origin-list',
  templateUrl: './origin-list.component.html',
  styleUrls: ['./origin-list.component.scss']
})
export class ClientOriginListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'description', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private excelService: ExporterService,
    private _service: ClientOriginService
  ) { }

  ngOnInit() {
    
    this._service.getAll().subscribe(res => {
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/client-origins/new']);
  }

  onShow(data: ClientOriginI) {
    this.router.navigate(['admin/client-origins/' + data.id]);
  }

  onEdit(data: ClientOriginI) {
    this.router.navigate(['admin/client-origins/edit/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._service.deleted(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Elemento borrado.',
              'success'
            )
            this.router.navigate(['admin/client-origins']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_origenes_cliente');
  }

}
