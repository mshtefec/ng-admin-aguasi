import { Component, OnInit }                  from '@angular/core';
import { Router }                             from '@angular/router';

import { ToastrService }                      from 'ngx-toastr';
import { ApiRestService }                     from '@services/api-rest.service';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'clients' },
  ]
})
export class ClientFormComponent implements OnInit {

  title: string;
  isReadonly: Boolean = false;
  isNew: Boolean = false;

  provinces: any = [
    { "name": "Ciudad Autónoma de Buenos Aires", "country": "AR" },
    { "name": "Buenos Aires", "country": "AR" },
    { "name": "Catamarca", "country": "AR" },
    { "name": "Chaco", "country": "AR" },
    { "name": "Chubut", "country": "AR" },
    { "name": "Córdoba", "country": "AR" },
    { "name": "Corrientes", "country": "AR" },
    { "name": "Entre Ríos", "country": "AR" },
    { "name": "Formosa", "country": "AR" },
    { "name": "Jujuy", "country": "AR" },
    { "name": "La Pampa", "country": "AR" },
    { "name": "La Rioja", "country": "AR" },
    { "name": "Mendoza", "country": "AR" },
    { "name": "Misiones", "country": "AR" },
    { "name": "Neuquén", "country": "AR" },
    { "name": "Río Negro", "country": "AR" },
    { "name": "Salta", "country": "AR" },
    { "name": "San Juan", "country": "AR" },
    { "name": "San Luis", "country": "AR" },
    { "name": "Santa Cruz", "country": "AR" },
    { "name": "Santa Fe", "country": "AR" },
    { "name": "Santiago del Estero", "country": "AR" },
    { "name": "Tierra del Fuego, Antártida e Islas del Atlántico Sur", "country": "AR" },
    { "name": "Tucumán", "country": "AR" }
  ]

  client: any;

  users: any[];
  zones: any[];
  states: any[];
  origins: any[];

  constructor(
    private _api: ApiRestService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() { 

    if (!this.client) {
      this.isNew = true;
      this.client = { 
        seller: [],
        zone: [],
        status: [],
        origin: [],
        high_date: []
      };
    }

    // usuario logueado
    // let user = JSON.parse(localStorage.getItem('identity'));

    this._api.getAllFrom('users').subscribe( res => {
      this.users = res;
    });

    this._api.getAllFrom('zones').subscribe( res => {
      this.zones = res;
    });

    this._api.getAllFrom('client-status').subscribe( res => {
      this.states = res;
    });

    this._api.getAllFrom('client-origin').subscribe( res => {
      this.origins = res;
    });
  }

  onSubmit(form: any) {
    if (this.isNew) {
      this.newRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  newRecord(form: any) {
    this._api.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Cliente añadido');
        this.router.navigate(['admin/clients']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }
  
  updateRecord(form: any) {
    this._api.update(form).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Cliente modificado');
        this.router.navigate(['admin/clients']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }

}
