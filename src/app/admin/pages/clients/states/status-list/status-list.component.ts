import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatDialog }                      from '@angular/material/dialog';
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import Swal                               from 'sweetalert2';

import { ClientStatusI }                  from '@app/interfaces/client-status.interface';

import { ClientStatusService }            from '@app/services/client-status.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.scss']
})
export class ClientStatusListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'description', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private excelService: ExporterService,
    private _service: ClientStatusService
  ) { }

  ngOnInit() {
    
    this._service.getAll().subscribe(res => {
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/client-states/new']);
  }

  onShow(data: ClientStatusI) {
    this.router.navigate(['admin/client-states/' + data.id]);
  }

  onEdit(data: ClientStatusI) {
    this.router.navigate(['admin/client-states/edit/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._service.deleted(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Elemento borrado.',
              'success'
            )
            this.router.navigate(['admin/client-states']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_estados_cliente');
  }

}
