import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { ClientStatusI }                      from '../../../../../interfaces/client-status.interface';

import { ToastrService }                      from 'ngx-toastr';
import { ClientStatusService }                from '@app/services/client-status.service';

@Component({
  selector: 'app-status-edit',
  templateUrl: './status-edit.component.html',
  styleUrls: ['./status-edit.component.scss']
})
export class ClientStatusEditComponent implements OnInit {

  id: number;

  form = new FormGroup({
    id: new FormControl(this.id, [Validators.required]),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _service: ClientStatusService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.id = +params['id'];

        this._service.getOne(this.id).subscribe(
          response => {
            this.form.controls.id.setValue(response.id);
            this.form.controls.name.setValue(response.name);
            this.form.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: ClientStatusI) {
    this.updateRecord(form);
  }

  updateRecord(form: ClientStatusI) {

    this._service.update(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Elemento actualizado');
        this.router.navigate(['admin/client-states']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/client-states']);
  }

}
