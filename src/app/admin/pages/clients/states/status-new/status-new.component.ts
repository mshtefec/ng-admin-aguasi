import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';
import { ClientStatusI }                      from '@app/interfaces/client-status.interface';
import { ClientStatusService }                from '@app/services/client-status.service';
import { ToastrService }                      from 'ngx-toastr';

@Component({
  selector: 'app-status-new',
  templateUrl: './status-new.component.html',
  styleUrls: ['./status-new.component.scss']
})
export class ClientStatusNewComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  constructor(
    private _service: ClientStatusService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() { }

  onSubmit(form: ClientStatusI) {
    this.insertRecord(form);
  }


  insertRecord(form: ClientStatusI) {
    this._service.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Elemento añadido');
        this.resetForm();
        this.router.navigate(['admin/client-states']);
      },
      error => {
        console.error(error);
      }
    );
  }

  resetForm() {
    this.form.reset();
  }

  onBack() {
    this.router.navigate(['admin/client-states']);
  }

}
