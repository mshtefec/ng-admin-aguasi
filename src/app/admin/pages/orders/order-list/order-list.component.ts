import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router, ActivatedRoute }         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort, MatSortable }           from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ExporterService }                from '@app/services/exporter.service';

import { OrderFormComponent }             from '../order-form/order-form.component';

import { NbDialogService }                from '@nebular/theme';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'client_id', 'client_name', 'client_lastname','total', 'delivery_date', 'load_date', 'created_at', 'status', 'actions'];
  dataSource = new MatTableDataSource();
  dataSource2 = new MatTableDataSource();

  filterClient: number;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor (
    public dialog: MatDialog,
    private excelService: ExporterService,
    private _orderService: OrderService,
    public router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private dialogService: NbDialogService
  ) {
    
  }

  ngOnInit() {

    this.route.params.subscribe(
      params => {
        this.filterClient = +params['id'];
        if (this.filterClient) {
          this.applyFilter(String(this.filterClient));
        }
      }
    );

    this._orderService.getAllWithNames().subscribe(res => {

      this.dataSource.data = res;
      this.dataSource2.data = res;
      // muestra todos los ultimos pedidos
      this.sort.sort(({ id: 'id', start: 'desc'}) as MatSortable);
      this.dataSource.sort = this.sort;
    });
  }

  getTotal() {
    const orders: OrderI[] = this.dataSource.data;
    return orders.map(t => t.total).reduce((acc, value) => Number(acc) + Number(value), 0);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.dialogService.open(OrderFormComponent, {
      context: {
        title: 'Nuevo pedido',
      },
    });
  }

  onEdit(element: any) {
    this.dialogService.open(OrderFormComponent, {
      context: {
        title: 'Editando pedido',
        order: element
      },
    });
  }

  onShow(element: any) {
    this.dialogService.open(OrderFormComponent, {
      context: {
        title: 'Detalles del pedido',
        order: element,
        isReadonly: true
      },
    });
  }

  onFinish(order: OrderI) {

    Swal.fire({

      title: 'estas seguro que desea marcar como vendido el pedido?',
      text: 'al confirmar este pedido aparecera en el modulo de ventas también!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Finalizar!'

    }).then(result => {

      if (result.value) {

        this._orderService.sellOne(order).subscribe( 
          response => {
            this.toastr.success('Cambios guardados', 'Pedido actualizado');
            this.router.navigate(['admin/orders']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._orderService.deletedOrder(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Orden borrada.',
              'success'
            )
            this.router.navigate(['admin/orders']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource2.data, 'listado_de_pedidos');
  }

}
