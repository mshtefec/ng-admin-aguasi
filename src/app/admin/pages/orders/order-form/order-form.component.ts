import { Component, OnInit }                    from '@angular/core';
import { Router }                               from '@angular/router';
import { FormBuilder, Validators }              from '@angular/forms';

import { MatTableDataSource }                   from '@angular/material/table';

import { SupplyI }                              from 'src/app/interfaces/supply.interface';
import { ClientI }                              from 'src/app/interfaces/client.interface';

import { ToastrService }                        from 'ngx-toastr';
import { ApiRestService }                       from '@services/api-rest.service';
import { ClientService }                        from '@services/client.service';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'orders' },
  ]
})
export class OrderFormComponent implements OnInit {

  title: string;
  isReadonly: Boolean = false;
  isNew: Boolean = false;

  order: any;
  totalOrder: number = 0;

  users: any[];
  clients: any[];

  statuses: any[] = [
    { id: 'PENDIENTE', name: 'Pendiente'},
    { id: 'ENTREGADO', name: 'Entregado'},
    { id: 'DISTRIBUCION', name: 'Distribución'},
    { id: 'VENDIDO', name: 'Vendido'},
  ];

  displayedColumnsSupply: string[] = ['id', 'name', 'description', 'unit_p', 'quantity', 'total', 'actions'];
  dataSourceSupplies = new MatTableDataSource();

  supplyForm = this.fb.group({
    supply: ['', Validators.required],
    quantity: ['',[Validators.min(1)]],
  });

  suppliesCollection: SupplyI[];
  suppliesSelecteds: any[] = [];

  constructor(
    private _api: ApiRestService,
    private _clients: ClientService,
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() { 

    if (!this.order) {
      this.isNew = true;
      this.order = { 
        client: [],
        seller: [],
        delivery: [],
        supplies: [],
        delivery_date: [],
        created_at: [],
        load_date: [],
        address: '',
        total: 0
      };
    } else {
      this.suppliesSelecteds = this.order.supplies;
      const dateMaping: any = this.suppliesSelecteds;
      dateMaping.forEach(m => {
        m.id = m.supply.id,
        m.name = m.supply.name,
        m.description = m.supply.description,
        m.unit_p = m.supply.price,
        m.quantity = m.quantity,
        m.total = Number(m.supply.price * m.quantity).toFixed(2)
      });

      this.dataSourceSupplies.data = dateMaping;
    }

    this._api.getAllFrom('users').subscribe( res => {
      this.users = res;
    });

    this._clients.getClientsOrderByLastname().subscribe( res => {
      this.clients = res;
    });

    this._api.getAllFrom('supplies').subscribe( data => {
      const filter = data.filter(
        t => t.type == 0 && t.active == 1
      );
      const dateMaping: any = filter;

      dateMaping.forEach(m => {
        m.created_at = m.created_at.date,
        m.updated_at = m.updated_at.date
      });

      this.suppliesCollection = dateMaping;
    });

  }

  updateClientInfo(client: ClientI) {
    const clientInfo = this.clients.filter(
      c => c.id == client
    );
    this.order.address = clientInfo[0].address;
  }


  addNewSupply(supplyForm: any) {
    
    this.suppliesSelecteds.push(supplyForm);
    
    const dateMaping: any = this.suppliesSelecteds;
    dateMaping.forEach(m => {
      m.id = m.supply.id,
      m.name = m.supply.name,
      m.description = m.supply.description,
      m.unit_p = m.supply.price,
      m.quantity = m.quantity,
      m.total = Number(m.supply.price * m.quantity).toFixed(2)
    });

    this.dataSourceSupplies.data = dateMaping;
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder += Number(supplyForm.supply.price) * supplyForm.quantity;
    this.order.total = this.totalOrder;

    this.supplyForm.reset();
  }

  onDeleted(id: number, total: number) {
    
    this.dataSourceSupplies.data.splice(this.dataSourceSupplies.data.indexOf(id), 1);
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder -= Number(total);
    this.order.total = this.totalOrder;

    this.supplyForm.reset();
  }

  onSubmit(form: any) {
    form.supplies = this.suppliesSelecteds;
    console.log(form);
    if (this.isNew) {
      this.newRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  newRecord(form: any) {
    this._api.add(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Pedido añadido');
        this.router.navigate(['admin/orders']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }
  
  updateRecord(form: any) {
    this._api.update(form).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Pedido modificado');
        this.router.navigate(['admin/orders']);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      },
      error => {
        console.error(error);
      }
    );
  }

}

