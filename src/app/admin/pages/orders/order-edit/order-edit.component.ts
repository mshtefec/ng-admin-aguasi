import { Component, OnInit }                  from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';
import { DatePipe }                           from '@angular/common';

import { MatTableDataSource }                 from '@angular/material/table';

import { OrderI }                             from 'src/app/interfaces/order.interface';
import { ClientI }                            from 'src/app/interfaces/client.interface';
import { UserI }                              from 'src/app/interfaces/user.interface';
import { SupplyI }                            from 'src/app/interfaces/supply.interface';


import { ToastrService }                      from 'ngx-toastr';
import { ClientService }                      from '@app/services/client.service';
import { UserService }                        from '@app/services/user.service';
import { SupplyService }                      from '@app/services/supply.service';
import { OrderService }                       from '@app/services/order.service';

import { Observable }                         from 'rxjs';
import { map, startWith }                     from 'rxjs/operators';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {

  orderId: number;
  totalOrder: number = 0;

  displayedColumnsSupply: string[] = ['id', 'name', 'description', 'unit_p', 'quantity', 'total', 'actions'];
  dataSourceSupplies = new MatTableDataSource();

  filterFieldClient = 'lastname';
  filterFieldDeliverySeller = 'email';
  filterFieldSupply = 'name';

  clients$: Observable<ClientI[]>;
  sellers$: Observable<UserI[]>;
  deliveries$: Observable<UserI[]>;
  supplies$: Observable<SupplyI[]>;

  suppliesCollection: SupplyI[];

  suppliesSelecteds: any[] = [];

  clientInfo: ClientI;
  sellerInfo: UserI;
  deliveryInfo: UserI;
  supplyInfo: SupplyI;
  deliveryDateInfo: String;
  addressInfo: String;
  statusInfo: String;

  orderForm = this.fb.group({
    id: ['', Validators.required],
    client: ['', Validators.required],
    seller: ['', Validators.required],
    delivery: ['', Validators.required],
    delivery_date: ['', Validators.required],
    description: ['', [Validators.required, Validators.minLength(3)]],
    address: ['', Validators.required],
    status: ['', Validators.required],
    total: [{value: ''}, Validators.required],
    supply: ['', Validators.required],
    quantity: ['1',[Validators.min(1)]],
    supplies: this.fb.array([])
  });

  supplyForm = this.fb.group({
    supply: ['', Validators.required],
    quantity: ['',[Validators.min(1)]],
  });
  
  client = new FormControl();
  seller = new FormControl();
  delivery = new FormControl();
  supply = new FormControl();
  
  options: string[] = ['One', 'Two', 'Three'];
  
  filteredClients: Observable<string[]>;
  filteredSellers: Observable<string[]>;
  filteredDeliveries: Observable<string[]>;
  filteredSupplies: Observable<string[]>;

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private _clientService: ClientService,
    private _userService: UserService,
    private _supplyService: SupplyService,
    private _orderService: OrderService
  ) {
  }

  getOrder() {
    this.route.params.subscribe(
      params => {
        this.orderId = +params['id'];

        this._orderService.getOrder(this.orderId).subscribe(
          response => {
            this.clientInfo = response.client;
            this.sellerInfo = response.seller;
            this.deliveryInfo = response.delivery;
            this.supplyInfo = response.supply;
            this.deliveryDateInfo = this.datePipe.transform(response.delivery_date.date, 'dd/MM/yyyy');
            this.addressInfo = response.address;
            this.statusInfo = response.status;

            this.suppliesSelecteds = response.supplies
            const dateMaping: any = this.suppliesSelecteds;
            dateMaping.forEach(m => {
              m.id = m.supply.id,
              m.name = m.supply.name,
              m.description = m.supply.description,
              m.unit_p = m.supply.price,
              m.quantity = m.quantity,
              m.total = Number(m.supply.price * m.quantity).toFixed(2)
            });

            this.dataSourceSupplies.data = dateMaping;
            
            this.orderForm.controls.id.setValue(response.id);
            this.orderForm.controls.client.setValue(response.client);
            this.orderForm.controls.seller.setValue(response.seller);
            this.orderForm.controls.delivery.setValue(response.delivery);
            this.orderForm.controls.address.setValue(response.address);
            this.orderForm.controls.status.setValue(response.status);
            this.orderForm.controls.supply.setValue(response.supply);
            this.orderForm.controls.quantity.setValue(response.quantity);
            this.orderForm.controls.total.setValue(response.total);
            this.orderForm.controls.delivery_date.setValue(response.delivery_date);
            this.orderForm.controls.description.setValue(response.description);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  ngOnInit() {

    this.filteredClients = this.client.valueChanges.pipe(
      startWith(''),
      map(
        value => this._filter(value)
      )
    );

    this.filteredSellers = this.seller.valueChanges.pipe(
      startWith(''),
      map(
        value => this._filter(value)
      )
    );

    this.filteredDeliveries = this.delivery.valueChanges.pipe(
      startWith(''),
      map(
        value => this._filter(value)
      )
    );

    this._supplyService.getSupplies().subscribe( data => {
      const filter = data.filter(
        t => t.type == 0 && t.active == 1
      );
      const dateMaping: any = filter;

      dateMaping.forEach(m => {
        m.created_at = m.created_at.date,
        m.updated_at = m.updated_at.date
      });

      this.suppliesCollection = dateMaping;
    });

    this.getOrder();

    this.clients$ = this._clientService.getClients();
    this.sellers$ = this._userService.getUsers();
    this.deliveries$ = this._userService.getUsers();
    this.supplies$ = this._supplyService.getSupplies();

  }

  onSubmit(form: any) {
    this.insertRecord(form);
  }

  addNewSupply(supplyForm: any) {
    
    this.suppliesSelecteds.push(supplyForm);
    
    const dateMaping: any = this.suppliesSelecteds;
    dateMaping.forEach(m => {
      m.id = m.supply.id,
      m.name = m.supply.name,
      m.description = m.supply.description,
      m.unit_p = m.supply.price,
      m.quantity = m.quantity,
      m.total = Number(m.supply.price * m.quantity).toFixed(2)
    });

    this.dataSourceSupplies.data = dateMaping;
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder += Number(supplyForm.supply.price) * supplyForm.quantity;
    this.orderForm.controls["total"].setValue(this.totalOrder);

    this.supplyForm.reset();
  }

  onDeleted(id: number, total: number) {
    
    this.dataSourceSupplies.data.splice(this.dataSourceSupplies.data.indexOf(id), 1);
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder -= Number(total);
    this.orderForm.controls["total"].setValue(this.totalOrder);

    this.supplyForm.reset();
  }

  insertRecord(form: any) {
    
    form.supplies = this.suppliesSelecteds;
    const order: OrderI = form;
    
    this._orderService.updateOrder(order).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Pedido Actualizado');
        // this.router.navigate(['admin/orders']);
      },
      error => {
        console.error(error);
      }
    );

  }

  updateClientInfo(client: ClientI){
    this.clientInfo = client;
    this.orderForm.controls.client.setValue(client);
    this.orderForm.controls.address.setValue(client.address);
  }

  updateSellerInfo(seller: UserI){
    this.sellerInfo = seller;
    this.orderForm.controls.seller.setValue(seller);
  }

  updateDeliveryInfo(delivery: UserI){
    this.deliveryInfo = delivery;
    this.orderForm.controls.delivery.setValue(delivery);
  }

  updateSupplyInfo(supply: SupplyI){
    this.supplyInfo = supply;
    this.totalOrder = Number(supply.price);
    this.orderForm.controls.quantity.setValue(1);
    this.orderForm.controls.total.setValue(Number(this.totalOrder));
  }

  updatePrice(quantity: number){
    this.totalOrder = this.supplyInfo.price * Number(quantity);
    this.orderForm.controls.total.setValue(Number(this.totalOrder));
  }

}