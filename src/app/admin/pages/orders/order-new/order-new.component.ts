import { Component, OnInit }                  from '@angular/core';
import { FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';

import { MatTableDataSource }                 from '@angular/material/table';

import { OrderI }                             from 'src/app/interfaces/order.interface';
import { ClientI }                            from 'src/app/interfaces/client.interface';
import { UserI }                              from 'src/app/interfaces/user.interface';
import { SupplyI }                            from 'src/app/interfaces/supply.interface';


import { ToastrService }                      from 'ngx-toastr';
import { ClientService }                      from '@app/services/client.service';
import { UserService }                        from '@app/services/user.service';
import { SupplyService }                      from '@app/services/supply.service';
import { OrderService }                       from '@app/services/order.service';

import { Observable }                         from 'rxjs';

@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.scss']
})
export class OrderNewComponent implements OnInit {

  orderId: string;
  totalOrder: number = 0;

  displayedColumnsSupply: string[] = ['id', 'name', 'description', 'unit_p', 'quantity', 'total', 'actions'];
  dataSourceSupplies = new MatTableDataSource();

  keyword = 'lastname';
  clients$: Observable<ClientI[]>;
  sellers: UserI[];
  deliveries: UserI[];
  suppliesCollection: SupplyI[];

  suppliesSelecteds: any[] = [];

  clientInfo: ClientI;
  sellerInfo: UserI;
  deliveryInfo: UserI;
  supplyInfo: SupplyI;

  orderForm = this.fb.group({
    client: ['', Validators.required],
    seller: ['', Validators.required],
    delivery: ['', Validators.required],
    description: ['', [Validators.required, Validators.minLength(3)]],
    address: ['', Validators.required],
    status: ['', Validators.required],
    total: [{value: ''}, Validators.required],
    supply: ['', Validators.required],
    quantity: ['1',[Validators.min(1)]],
    delivery_date: ['', Validators.required],
    load_date: ['', Validators.required],
    created_at: ['', Validators.required],
    supplies: this.fb.array([]),
  });

  supplyForm = this.fb.group({
    supply: ['', Validators.required],
    quantity: ['',[Validators.min(1)]],
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private _clientService: ClientService,
    private _userService: UserService,
    private _supplyService: SupplyService,
    private _orderService: OrderService
  ) {
  }

  ngOnInit() {

    this.clients$ = this._clientService.getClients();

    this._userService.getUsers().subscribe( data => {
      this.deliveries = data;
    });

    this._userService.getUsers().subscribe( data => {
      this.sellers = data;
    });

    this._supplyService.getSupplies().subscribe( data => {
      const filter = data.filter(
        t => t.type == 0 && t.active == 1
      );
      const dateMaping: any = filter;

      dateMaping.forEach(m => {
        m.created_at = m.created_at.date,
        m.updated_at = m.updated_at.date
      });

      this.suppliesCollection = dateMaping;
    });

  }

  onSubmit(form: any) {
    this.insertRecord(form);
  }

  // get suppliesForm() {
  //   return this.orderForm.get('supplies') as FormArray;
  // }

  // addNewSupplyForm() {
  //   const supplyForm = this.fb.group({
  //     supply: ['', Validators.required],
  //     quantity: ['1',[Validators.min(1)]],
  //   });
  //   this.suppliesForm.push(supplyForm);
  // }

  addNewSupply(supplyForm: any) {
    
    this.suppliesSelecteds.push(supplyForm);
    
    const dateMaping: any = this.suppliesSelecteds;
    dateMaping.forEach(m => {
      m.id = m.supply.id,
      m.name = m.supply.name,
      m.description = m.supply.description,
      m.unit_p = m.supply.price,
      m.quantity = m.quantity,
      m.total = Number(m.supply.price * m.quantity).toFixed(2)
    });

    this.dataSourceSupplies.data = dateMaping;
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder += Number(supplyForm.supply.price) * supplyForm.quantity;
    this.orderForm.controls["total"].setValue(this.totalOrder);

    this.supplyForm.reset();
  }

  onDeleted(id: number, total: number) {
    
    this.dataSourceSupplies.data.splice(this.dataSourceSupplies.data.indexOf(id), 1);
    this.dataSourceSupplies._updateChangeSubscription();

    this.totalOrder -= Number(total);
    this.orderForm.controls["total"].setValue(this.totalOrder);

    this.supplyForm.reset();
  }

  insertRecord(form: any) {
    
    form.supplies = this.suppliesSelecteds;
    const order: OrderI = form;

    this._orderService.addOrder(order).subscribe(
      response => {
        this.toastr.success('Cambios guardados', 'Pedido Creado');
        this.router.navigate(['admin/orders']);
      },
      error => {
        console.error(error);
      }
    );

  }

  updateClientInfo(client: ClientI){
    this.clientInfo = client;
    this.orderForm.controls["address"].setValue(client.address);
    this.orderForm.controls["client"].setValue(client);
  }

  updateSellerInfo(seller: UserI){
    this.sellerInfo = seller;
  }

  updateDeliveryInfo(delivery: UserI){
    this.deliveryInfo = delivery;
  }

  updateSupplyInfo(supply: SupplyI){
    this.supplyInfo = supply;
    this.totalOrder += Number(supply.price);
    this.orderForm.controls["total"].setValue(this.totalOrder);
  }

  updatePrice(quantity: Number){
    this.totalOrder = this.supplyInfo.price * Number(quantity);
    this.orderForm.controls["total"].setValue(this.totalOrder);
  }

}