import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router, ActivatedRoute }         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { OrderService }                   from '@services/order.service';
import { OrderI }                         from '@interfaces/order.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-ordersupply-list',
  templateUrl: './ordersupply-list.component.html',
  styleUrls: ['./ordersupply-list.component.scss']
})
export class OrderSupplyListComponent implements OnInit {

  orders: OrderI[];
  suppliesSelecteds: any[] = [];

  displayedColumns: string[] = ['id_order', 'id_supply', 'name', 'description', 'unit_p', 'quantity', 'total'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor (
    public dialog: MatDialog,
    private excelService: ExporterService,
    private _orderService: OrderService,
    public router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    
  }

  ngOnInit() {

    this._orderService.getOrders().subscribe(res => {
      this.orders = res;
      this.orders.forEach(o => {
        
        if (o.supplies.length != 0) {
          o.supplies.forEach(s => {
            this.suppliesSelecteds.push(s);
          });
        } else {
          const supplyOrd: any = {
            order: o,
            supply: o.supply,
            quantity: o.quantity
          } 
          this.suppliesSelecteds.push(supplyOrd);
        }
        
      });

      const dateMaping: any = this.suppliesSelecteds;
      dateMaping.forEach(m => {
        m.id_order = m.order.id,
        m.id_supply = m.supply.id,
        m.name = m.supply.name,
        m.description = m.supply.description,
        m.unit_p = m.supply.price,
        m.quantity = m.quantity,
        m.total = Number(m.supply.price * m.quantity).toFixed(2)
      });

      this.dataSource.data = dateMaping;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_detalles_pedidos_articulos');
  }

  onBack() {
    this.router.navigate(['admin/orders']);
  }

}
