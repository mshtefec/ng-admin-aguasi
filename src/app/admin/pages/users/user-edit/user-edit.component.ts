import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { UserI }                              from '../../../../interfaces/user.interface';

import { ToastrService }                      from 'ngx-toastr';
import { UserService }                        from '@app/services/user.service';

interface Rol {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  roles: Rol[] = [
    { value: 'ROLE_ADMIN', viewValue: 'ADMIN' },
    { value: 'ROLE_VENDEDOR', viewValue: 'VENDEDOR' },
    { value: 'ROLE_REPARTIDOR', viewValue: 'REPARTIDOR' },
  ];

  userId: number;

  userForm = new FormGroup({
    id: new FormControl(this.userId, [Validators.required]),
    username: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _userService: UserService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.userId = +params['id'];

        this._userService.getUser(this.userId).subscribe(
          response => {
            this.userForm.controls.id.setValue(response.id);
            this.userForm.controls.username.setValue(response.username);
            this.userForm.controls.firstname.setValue(response.firstname);
            this.userForm.controls.lastname.setValue(response.lastname);
            this.userForm.controls.email.setValue(response.email);
            this.userForm.controls.role.setValue(response.role);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onSubmit(form: UserI) {
    this.updateRecord(form);
  }

  updateRecord(form: UserI) {

    this._userService.updateUser(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Usuario actualizado');
        this.router.navigate(['admin/users']);
      },
      error => {
        console.error(error);
      }
    );

  }

  onBack() {
    this.router.navigate(['admin/users']);
  }

}
