import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';
import { UserI }                              from '@interfaces/user.interface';
import { UserService }                        from '@app/services/user.service';
import { ToastrService }                      from 'ngx-toastr';

interface Rol {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.scss']
})
export class UserNewComponent implements OnInit {

  roles: Rol[] = [
    { value: 'ROLE_ADMIN', viewValue: 'ADMIN' },
    { value: 'ROLE_VENDEDOR', viewValue: 'VENDEDOR' },
    { value: 'ROLE_REPARTIDOR', viewValue: 'REPARTIDOR' },
  ];
  public selectedDefaultRole = this.roles[0].value;

  userForm = new FormGroup({
    username: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private _userService: UserService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() { }

  onSubmit(form: UserI) {
    this.insertRecord(form);
  }


  insertRecord(form: UserI) {
    this._userService.addUser(form).subscribe( 
      response => {
        this.toastr.success('Cambios guardados', 'Usuario añadido');
        this.resetForm();
        this.router.navigate(['admin/users']);
      },
      error => {
        console.error(error);
      }
    );
  }

  resetForm() {
    this.userForm.reset();
  }

  onBack() {
    this.router.navigate(['admin/users']);
  }

}
