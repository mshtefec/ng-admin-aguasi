import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatDialog }                      from '@angular/material/dialog';
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import Swal                               from 'sweetalert2';

import { UserI }                          from '@interfaces/user.interface';

import { UserService }                    from '@app/services/user.service';
import { ExporterService }                from '@app/services/exporter.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'username', 'firstname', 'lastname', 'email', 'role', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private excelService: ExporterService,
    private _userService: UserService
  ) { }

  ngOnInit() {
    
    this._userService.getUsers().subscribe(res => {

      const dateMaping: any = res;

      dateMaping.forEach(m => {
        m.created_at = m.created_at.date,
        m.updated_at = m.updated_at.date
      });

      this.dataSource.data = dateMaping;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/users/new']);
  }

  onShow(data: UserI) {
    this.router.navigate(['admin/users/' + data.id]);
  }

  onEdit(data: UserI) {
    this.router.navigate(['admin/users/edit/' + data.id]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._userService.deletedUser(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Usuario borrado.',
              'success'
            )
            this.router.navigate(['admin/users']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 3000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

  exportToExcel() {
    this.excelService.exportToExcel(this.dataSource.data, 'listado_de_usuarios');
  }

}
