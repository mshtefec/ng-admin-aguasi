import { Component, OnInit }                  from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { ToastrService }                      from 'ngx-toastr';
import { UserService }                        from '@app/services/user.service';

@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.scss']
})
export class UserShowComponent implements OnInit {

  userId: number;

  userForm = new FormGroup({
    id: new FormControl(this.userId, [Validators.required]),
    username: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _userService: UserService
  ) { }

  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.userId = +params['id'];

        this._userService.getUser(this.userId).subscribe(
          response => {
            this.userForm.controls.id.setValue(response.id);
            this.userForm.controls.username.setValue(response.username);
            this.userForm.controls.firstname.setValue(response.firstname);
            this.userForm.controls.lastname.setValue(response.lastname);
            this.userForm.controls.email.setValue(response.email);
            this.userForm.controls.role.setValue(response.role);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/users']);
  }

}
