import { NgModule }                     from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';

import { AdminComponent }               from '@admin/admin.component';

import { ReportComponent }              from '@admin/pages/dashboard/report/report.component';

import { UserListComponent }            from '@admin/pages/users/user-list/user-list.component';
import { UserNewComponent }             from '@admin/pages/users/user-new/user-new.component';
import { UserEditComponent }            from '@admin/pages/users/user-edit/user-edit.component';
import { UserShowComponent }            from '@admin/pages/users/user-show/user-show.component';

import { ClientListComponent }          from '@admin/pages/clients/client-list/client-list.component';

import { ZoneListComponent }            from '@admin/pages/zones/zone-list/zone-list.component';
import { ZoneNewComponent }             from '@admin/pages/zones/zone-new/zone-new.component';
import { ZoneEditComponent }            from '@admin/pages/zones/zone-edit/zone-edit.component';
import { ZoneShowComponent }            from '@admin/pages/zones/zone-show/zone-show.component';

import { ClientStatusListComponent }    from './pages/clients/states/status-list/status-list.component';
import { ClientStatusNewComponent }     from './pages/clients/states/status-new/status-new.component';
import { ClientStatusEditComponent }    from './pages/clients/states/status-edit/status-edit.component';
import { ClientStatusShowComponent }    from './pages/clients/states/status-show/status-show.component';

import { ClientOriginShowComponent }    from './pages/clients/origins/origin-show/origin-show.component';
import { ClientOriginNewComponent }     from './pages/clients/origins/origin-new/origin-new.component';
import { ClientOriginEditComponent }    from './pages/clients/origins/origin-edit/origin-edit.component';
import { ClientOriginListComponent }    from './pages/clients/origins/origin-list/origin-list.component';

import { SupplyListComponent }          from '@admin/pages/supplies/supply-list/supply-list.component';
import { SupplyNewComponent }           from '@admin/pages/supplies/supply-new/supply-new.component';
import { SupplyEditComponent }          from '@admin/pages/supplies/supply-edit/supply-edit.component';
import { SupplyShowComponent }          from '@admin/pages/supplies/supply-show/supply-show.component';

import { CostListComponent }            from '@admin/pages/expenses/cost-list/cost-list.component';
import { CostNewComponent }             from '@admin/pages/expenses/cost-new/cost-new.component';
import { CostEditComponent }            from '@admin/pages/expenses/cost-edit/cost-edit.component';
import { CostShowComponent }            from '@admin/pages/expenses/cost-show/cost-show.component';

import { CategoryExpListComponent }     from '@admin/pages/expenses/categories/categoryexp-list/categoryexp-list.component';
import { CategoryExpNewComponent }      from '@admin/pages/expenses/categories/categoryexp-new/categoryexp-new.component';
import { CategoryExpEditComponent }     from '@admin/pages/expenses/categories/categoryexp-edit/categoryexp-edit.component';
import { CategoryExpShowComponent }     from '@admin/pages/expenses/categories/categoryexp-show/categoryexp-show.component';

import { OrderListComponent }           from '@admin/pages/orders/order-list/order-list.component';
import { OrderNewComponent }            from '@admin/pages/orders/order-new/order-new.component';
import { OrderEditComponent }           from '@admin/pages/orders/order-edit/order-edit.component';
import { OrderShowComponent }           from '@admin/pages/orders/order-show/order-show.component';
import { OrderFormComponent }           from '@admin/pages/orders/order-form/order-form.component';

import { OrderSupplyListComponent }     from '@admin/pages/orders/order-supply/ordersupply-list.component';

import { SaleListComponent }            from '@admin/pages/sales/sale-list/sale-list.component';
import { SaleShowComponent }            from '@admin/pages/sales/sale-show/sale-show.component';

import { BottledListComponent }         from '@admin/pages/supplies/bottles/bottled-list/bottled-list.component';
import { BottledFormComponent }         from '@admin/pages/supplies/bottles/bottled-form/bottled-form.component';

const routes: Routes = [
  // { path: '', redirectTo: 'admin', pathMatch: 'full' },
  { 
    path: '', component: AdminComponent, children: [
      
      { path: 'reports',            component: ReportComponent, },
      
      { path: 'users',              component: UserListComponent, },
      { path: 'users/new',          component: UserNewComponent, },
      { path: 'users/edit/:id',     component: UserEditComponent, },
      { path: 'users/:id',          component: UserShowComponent, },
      
      { path: 'clients',            component: ClientListComponent, },

      { path: 'zones',              component: ZoneListComponent, },
      { path: 'zones/new',          component: ZoneNewComponent, },
      { path: 'zones/edit/:id',     component: ZoneEditComponent, },
      { path: 'zones/:id',          component: ZoneShowComponent, },

      { path: 'client-states',          component: ClientStatusListComponent, },
      { path: 'client-states/new',      component: ClientStatusNewComponent, },
      { path: 'client-states/edit/:id', component: ClientStatusEditComponent, },
      { path: 'client-states/:id',      component: ClientStatusShowComponent, },

      { path: 'client-origins',          component: ClientOriginListComponent, },
      { path: 'client-origins/new',      component: ClientOriginNewComponent, },
      { path: 'client-origins/edit/:id', component: ClientOriginEditComponent, },
      { path: 'client-origins/:id',      component: ClientOriginShowComponent, },

      { path: 'client-history/:id',      component: OrderListComponent, },
      
      { path: 'supplies',           component: SupplyListComponent, },
      { path: 'supplies/new',       component: SupplyNewComponent, },
      { path: 'supplies/edit/:id',  component: SupplyEditComponent, },
      { path: 'supplies/:id',       component: SupplyShowComponent, },

      { path: 'bottles-list',            component: BottledListComponent, },
      { path: 'supplies/bottles/form',   component: BottledFormComponent, },

      { path: 'expenses',           component: CostListComponent, },
      { path: 'expenses/new',       component: CostNewComponent, },
      { path: 'expenses/edit/:id',  component: CostEditComponent, },
      { path: 'expenses/:id',       component: CostShowComponent, },

      { path: 'categories',           component: CategoryExpListComponent, },
      { path: 'categories/new',       component: CategoryExpNewComponent, },
      { path: 'categories/edit/:id',  component: CategoryExpEditComponent, },
      { path: 'categories/:id',       component: CategoryExpShowComponent, },

      { path: 'orders',             component: OrderListComponent, },
      { path: 'orders/new',         component: OrderNewComponent, },
      { path: 'orders/edit/:id',    component: OrderEditComponent, },
      { path: 'orders/:id',         component: OrderShowComponent, },

      { path: 'orders-supplies',    component: OrderSupplyListComponent, },
      { path: 'order-form',         component: OrderFormComponent, },
      
      { path: 'sales',              component: SaleListComponent, },
      { path: 'sales/:id',          component: SaleShowComponent, },
    
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
