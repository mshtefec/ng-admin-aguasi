// MODULES
import { NgModule }                           from '@angular/core';
import { CommonModule, DatePipe  }            from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AdminRoutingModule }                 from '@admin/admin-routing.module';
// THIRDS
import { AutocompleteLibModule }              from 'angular-ng-autocomplete';
import { NgxMaskModule }                      from 'ngx-mask';
import { ToastrModule }                       from 'ngx-toastr';
import { MaterialModule }                     from '../thirdmodules/material.module';
import { NbEvaIconsModule }                   from '@nebular/eva-icons';  
import {
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbSelectModule,
  NbMenuModule,
  NbDatepickerModule,
  NbIconModule,
  NbButtonModule,
  NbCardModule,
  NbActionsModule,
  NbInputModule,
  NbFormFieldModule,
  NbListModule,
  NbBadgeModule,
  NbToggleModule,
  NbCheckboxModule,
  NbDialogModule,
} from '@nebular/theme';

// COMPONENTS
import { AdminComponent }                     from '@admin/admin.component';
import { SidebarComponent }                   from '@admin/sidebar/sidebar.component';
import { ReportComponent }                    from '@admin/pages/dashboard/report/report.component';

import { UserListComponent }                  from '@admin/pages/users/user-list/user-list.component';
import { UserNewComponent }                   from '@admin/pages/users/user-new/user-new.component';
import { UserEditComponent }                  from '@admin/pages/users/user-edit/user-edit.component';
import { UserShowComponent }                  from '@admin/pages/users/user-show/user-show.component';

import { ClientListComponent }                from '@admin/pages/clients/client-list/client-list.component';
import { ClientFormComponent }                from '@app/admin/pages/clients/client-form/client-form.component';

import { ZoneListComponent }                  from '@admin/pages/zones/zone-list/zone-list.component';
import { ZoneNewComponent }                   from '@admin/pages/zones/zone-new/zone-new.component';
import { ZoneEditComponent }                  from '@admin/pages/zones/zone-edit/zone-edit.component';
import { ZoneShowComponent }                  from '@admin/pages/zones/zone-show/zone-show.component';

import { ClientStatusShowComponent }          from './pages/clients/states/status-show/status-show.component';
import { ClientStatusNewComponent }           from './pages/clients/states/status-new/status-new.component';
import { ClientStatusEditComponent }          from './pages/clients/states/status-edit/status-edit.component';
import { ClientStatusListComponent }          from './pages/clients/states/status-list/status-list.component';

import { ClientOriginShowComponent }          from './pages/clients/origins/origin-show/origin-show.component';
import { ClientOriginNewComponent }           from './pages/clients/origins/origin-new/origin-new.component';
import { ClientOriginEditComponent }          from './pages/clients/origins/origin-edit/origin-edit.component';
import { ClientOriginListComponent }          from './pages/clients/origins/origin-list/origin-list.component';

import { SupplyListComponent }                from '@admin/pages/supplies/supply-list/supply-list.component';
import { SupplyNewComponent }                 from '@admin/pages/supplies/supply-new/supply-new.component';
import { SupplyEditComponent }                from '@admin/pages/supplies/supply-edit/supply-edit.component';
import { SupplyShowComponent }                from '@admin/pages/supplies/supply-show/supply-show.component';

import { CostListComponent }                  from '@admin/pages/expenses/cost-list/cost-list.component';
import { CostNewComponent }                   from '@admin/pages/expenses/cost-new/cost-new.component';
import { CostEditComponent }                  from '@admin/pages/expenses/cost-edit/cost-edit.component';
import { CostShowComponent }                  from '@admin/pages/expenses/cost-show/cost-show.component';

import { CategoryExpListComponent }           from '@admin/pages/expenses/categories/categoryexp-list/categoryexp-list.component';
import { CategoryExpNewComponent }            from '@admin/pages/expenses/categories/categoryexp-new/categoryexp-new.component';
import { CategoryExpEditComponent }           from '@admin/pages/expenses/categories/categoryexp-edit/categoryexp-edit.component';
import { CategoryExpShowComponent }           from '@admin/pages/expenses/categories/categoryexp-show/categoryexp-show.component';

import { OrderListComponent }                 from '@admin/pages/orders/order-list/order-list.component';
import { OrderNewComponent }                  from '@admin/pages/orders/order-new/order-new.component';
import { OrderEditComponent }                 from '@admin/pages/orders/order-edit/order-edit.component';
import { OrderShowComponent }                 from '@admin/pages/orders/order-show/order-show.component';
import { OrderFormComponent }                 from '@admin/pages/orders/order-form/order-form.component';

import { OrderSupplyListComponent }           from '@admin/pages/orders/order-supply/ordersupply-list.component';

import { SaleListComponent }                  from '@admin/pages/sales/sale-list/sale-list.component';
import { SaleShowComponent }                  from '@admin/pages/sales/sale-show/sale-show.component';

import { BottledListComponent }               from '@admin/pages/supplies/bottles/bottled-list/bottled-list.component';
import { BottledFormComponent }               from '@admin/pages/supplies/bottles/bottled-form/bottled-form.component';

// SERVICES
import { AuthService }                        from '@services/auth.service';
import { UserService }                        from '@services/user.service';
import { ClientService }                      from '@services/client.service'
import { ZoneService }                        from '@services/zone.service';
import { ClientStatusService }                from '@app/services/client-status.service';
import { ClientOriginService }                from '@app/services/client-origin.service';
import { SupplyService }                      from '@services/supply.service';
import { ExporterService }                    from '@services/exporter.service';
import { CrudService }                        from '@services/crud.service';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      AdminRoutingModule,
      AutocompleteLibModule,
      NgxMaskModule.forRoot(),
      ToastrModule.forRoot(),
      NbDialogModule.forRoot(),
      MaterialModule,
      NbEvaIconsModule,

      NbThemeModule.forRoot(),
      NbLayoutModule,
      NbSidebarModule.forRoot(),
      NbSelectModule,
      NbMenuModule.forRoot(),
      NbDatepickerModule.forRoot(),
      NbIconModule,
      NbButtonModule,
      NbCardModule,
      NbActionsModule,
      NbInputModule,
      NbFormFieldModule,
      NbListModule,
      NbBadgeModule,
      NbToggleModule,
      NbCheckboxModule
    ],
    declarations: [      
      AdminComponent,
      SidebarComponent,
      ReportComponent,
      
      UserListComponent,
      UserNewComponent,
      UserEditComponent,
      UserShowComponent,
      
      ClientListComponent,
      ClientFormComponent,

      ZoneListComponent,
      ZoneNewComponent,
      ZoneEditComponent,
      ZoneShowComponent,

      ClientStatusListComponent,
      ClientStatusNewComponent,
      ClientStatusEditComponent,
      ClientStatusShowComponent,

      ClientOriginListComponent,
      ClientOriginNewComponent,
      ClientOriginEditComponent,
      ClientOriginShowComponent,
      
      SupplyListComponent,
      SupplyNewComponent,
      SupplyEditComponent,
      SupplyShowComponent,

      CostListComponent,
      CostNewComponent,
      CostEditComponent,
      CostShowComponent,

      CategoryExpListComponent,
      CategoryExpNewComponent,
      CategoryExpEditComponent,
      CategoryExpShowComponent,
      
      OrderListComponent,
      OrderNewComponent,
      OrderEditComponent,
      OrderShowComponent,
      OrderFormComponent,

      OrderSupplyListComponent,
      
      SaleListComponent,
      SaleShowComponent,
      
      BottledListComponent,
      BottledFormComponent
    ],
    providers: [
      DatePipe,
      AuthService,
      UserService,
      ClientService,
      ZoneService,
      ClientStatusService,
      ClientOriginService,
      SupplyService,
      ExporterService,
      CrudService
    ],
  })
  export class AdminModule { }